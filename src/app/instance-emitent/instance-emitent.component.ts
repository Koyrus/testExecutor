import { Component, OnInit } from '@angular/core';
import {Http, RequestOptions , Headers} from "@angular/http";
import {Router} from "@angular/router";
import {ConfirmationService, Message} from "primeng/primeng";
import {GlobalVariable} from "../global";

@Component({
  selector: 'app-instance-emitent',
  templateUrl: './instance-emitent.component.html',
  styleUrls: ['./instance-emitent.component.css']
})
export class InstanceEmitentComponent implements OnInit {
  public baseApiUrl = GlobalVariable.globalApiUrl;

  public emitent_result : any[] = [];
  display: boolean = false;
  msgs: Message[] = [];
  displayDialog: boolean;
  public emitent_edit : any[]=[];
  public new_emitent : boolean = false;
  public emitent_nom : any[]=[];

  constructor(private http : Http ,private router : Router , private confirmationService: ConfirmationService) {

  }

  ngOnInit() {
    var thisref = this;
    this.http.get(thisref.baseApiUrl+"/executor/admin/nomenclator/instance/emitent" , {
    }).subscribe(function (res) {
      console.log(res);
      thisref.emitent_result = res.json();
      console.log(thisref.emitent_result , "priority");

    })
  }
  showDialog(priority) {
    if(priority != null){
      this.emitent_edit = priority;
      this.new_emitent = false;
    }else {
      this.emitent_edit = [];
      this.new_emitent = true;
    }
    this.display = true;
  }


  saveEmitent(){
    var thisref = this;
    var headers = new Headers();
    headers.append("Accept", 'application/json');
    headers.append('Content-Type', 'application/json' );
    let options = new RequestOptions({ headers: headers });
    let postParams = {
      id_login_user: -1,
      id_nomenclator: this.emitent_edit['id'],
      name : this.emitent_edit['name'],
      type_nomenclator: "INSTANCE_EMITENT",

    };

    console.log(postParams , "postParams");


    this.http.post( thisref.baseApiUrl+"/executor/admin/nomenclator/save", postParams , options).subscribe(
      response => {
        this.emitent_nom = response.json();
        if(this.new_emitent){
          this.emitent_result.push(this.emitent_nom);
          this.msgs = [];
          this.msgs.push({severity:'success', summary:'Info Message', detail:'Successfuly'});
        }

      }, error => {
        console.log("err");
        this.msgs = [];
        this.msgs.push({severity:'error', summary:'Info Message', detail:'Error'});
      }
    );
    this.display = false;
    console.log(this.emitent_result);
  }

  public activatedNomInstance(instance){
    console.log(instance, "user");
    var headers = new Headers();
    headers.append("Accept", 'application/json');
    headers.append('Content-Type', 'application/json' );
    let options = new RequestOptions({ headers: headers });
    let postParams = {
      id_login_user: -1,
      id_nomenclator: instance.id,
      is_active: instance.is_active,
      type_nomenclator: "PAYMENT_MODE",
    };
    console.log(!instance.is_active, "!organization.is_active");
    this.http.post(this.baseApiUrl+"/executor/admin/nomenclator/active", postParams , options).subscribe(function (res) {
      console.log(res);
    });
  }

  deleteInstance(instance) {
    this.confirmationService.confirm({
      message: 'Do you want to delete this record?',
      header: 'Delete Confirmation?',
      icon: 'fa fa-trash',
      accept: () => {
        let postParams = {
          id_login_user: -1,
          id_nomenclator: instance.id,
          is_delete: true
        };
        var headers = new Headers();
        headers.append("Accept", 'application/json');
        headers.append('Content-Type', 'application/json' );
        let options = new RequestOptions({ headers: headers });
        console.log(postParams , "postParams");
        this.http.post(this.baseApiUrl+"/executor/admin/nomenclator/delete", postParams , options).subscribe(function (res) {
          console.log(res);
        });
        let index = this.emitent_result.indexOf(instance);
        console.log(index, "index");
        this.emitent_result = this.emitent_result.filter((val, i) => i != index);
        this.displayDialog = false;
        this.msgs = [{severity: 'info', summary: 'Confirmed', detail: 'Record deleted'}];
      },
      reject: () => {
        this.msgs = [{severity: 'info', summary: 'Rejected', detail: 'You have rejected'}];
      }
    });
  }

}
