import {ChangeDetectorRef, Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {Http, RequestOptions , Headers} from "@angular/http";
import {ConfirmationService, Message} from "primeng/primeng";
import {GlobalVariable} from "../global";

@Component({
  selector: 'app-organizations',
  templateUrl: './organizations.component.html',
  styleUrls: ['./organizations.component.css']
})
export class OrganizationsComponent implements OnInit {
  public baseApiUrl = GlobalVariable.globalApiUrl;

  private data: Response;
  id: null;
  public banks: any;
  msgs: Message[] = [];
  displayDialog: boolean;

  constructor(private router: Router, private http: Http, private route: ActivatedRoute, private confirmationService: ConfirmationService, private cd: ChangeDetectorRef) {
    var thisref= this;
    this.http.get(this.baseApiUrl+"/executor/admin/organization" , {
    }).subscribe(function (res) {
      thisref.results = res.json();
      console.log(thisref.results , "traw")
    })
  }

  public results: any[] = [];
  public total_recors: number = 0;
  private organizationId: any;


  public createOrganization() {
    this.router.navigateByUrl('adminDashboard/createOrganization');

    var headers = new Headers();
    headers.append("Accept", 'application/json');
    headers.append('Content-Type', 'application/json');

    let options = new RequestOptions({headers: headers});
    let postParams = {
      id_login_user: -1,
      id_organization: null,
    };

    console.log(postParams);
    var thisref = this;
    this.http.post(this.baseApiUrl+"/executor/admin/organization/create/edit", postParams, options)
      .subscribe(res => {
        thisref.banks = res.json();
        console.log(thisref.banks);
        localStorage.setItem("banks_For_CreateOrganization" , JSON.stringify(thisref.banks));
        if (res.status == 404) {
          console.log("error on post");
        }
      });
  }


  deleteOrganization(organization) {
    this.confirmationService.confirm({
      message: 'Do you want to delete this record?',
      header: 'Delete Confirmation?',
      icon: 'fa fa-trash',
      accept: () => {
        let postParams = {
          id_login_user: -1,
          id_organization: organization.id,
          is_delete: true
        };
        this.http.post(this.baseApiUrl+"/executor/admin/organization/delete", postParams).subscribe(function (res) {
          console.log(res);
        });
        let index = this.results.indexOf(organization);
        console.log(index, "index");
        this.results = this.results.filter((val, i) => i != index);
        this.displayDialog = false;
        this.msgs = [{severity: 'info', summary: 'Confirmed', detail: 'Record deleted'}];
      },
      reject: () => {
        this.msgs = [{severity: 'info', summary: 'Rejected', detail: 'You have rejected'}];
      }
    });
  }

  activatedOrganization(organization) {


    //
    // this.confirmationService.confirm({
    //   message: 'Do you want to delete this record?',
    //   header: 'Active Confirmation?',
    //   icon: 'fa fa-trash',
    //   accept: () => {
    console.log(organization, "organizations");

    let postParams = {
      id_login_user: -1,
      id: organization.id,
      is_active: organization.is_active
    };
    console.log(!organization.is_active, "!organization.is_active");
    this.http.post( this.baseApiUrl+"/executor/admin/organization/active", postParams).subscribe(function (res) {

      console.log(res);


    });
    //
    // organization.is_active = !organization.is_active;
    //   // let index = this.results.indexOf(organization);
    //   // console.log(index , "index");
    //   // this.results = this.results.filter((val,i) => i!=index);
    //   // this.displayDialog = false;
    //   this.msgs = [{severity:'info', summary:'Confirmed', detail:'Record deleted'}];
    // },
    // reject: () => {
    //   this.msgs = [{severity:'info', summary:'Rejected', detail:'You have rejected'}];


    // });
  }

  ngOnInit() {


  }



  public editOrganizationInTable(id:string){


    var headers = new Headers();
    headers.append("Accept", 'application/json');
    headers.append('Content-Type', 'application/json' );


    let options = new RequestOptions({ headers: headers });
    let postParams = {
      id_login_user : -1,
      id_organization: id,
    };

    console.log(postParams);
    this.http.post(this.baseApiUrl+"/executor/admin/organization/create/edit" , postParams , options )
      .subscribe(res =>{
        console.log(res);
        this.router.navigateByUrl('adminDashboard/editOrganization/'+id);
        var thisref= this;
        thisref.results = res.json();
        console.log(thisref.results);
        localStorage.setItem('data' , JSON.stringify(thisref.results));
      });
  }


}
