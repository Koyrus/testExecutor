import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DashcomponentComponent } from './dashcomponent.component';

describe('DashcomponentComponent', () => {
  let component: DashcomponentComponent;
  let fixture: ComponentFixture<DashcomponentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DashcomponentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DashcomponentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
