import { Component, OnInit } from '@angular/core';
import {Http, RequestOptions , Headers} from "@angular/http";
import {ActivatedRoute, Router} from "@angular/router";
import {GlobalVariable} from "../global";
import {toInt} from "ngx-bootstrap/bs-moment/utils/type-checks";
import {ConfirmationService, Message} from "primeng/primeng";

@Component({
  selector: 'app-detalii-dosar',
  templateUrl: './detalii-dosar.html',
  styleUrls: ['./detalii-dosar.css']
})
export class DetaliiDosar implements OnInit {

  constructor(private http : Http , private router : Router ,  private route : ActivatedRoute , private confirmationService: ConfirmationService ) {
    console.log(this.childrens , '111111111');
  }
  public editDosarID : number = null;
  public main_url = GlobalVariable.globalApiUrl;
  public result_details : any[] = [];
  public case_number : any;
  public nom_emitent_instance : string;
  public doc_executor : string;
  public created_date : any;
  public expired_date_case : string;
  public debitor_credentials : any[] = [];
  public creditor_credentials : any[] = [];
  public childrens : any[] =[];
  public file_content : any[]=[];
  public typeCase : string;
  msgs: Message[] = [];
  displayDialog: boolean;
  stateCase:string;

  EditFromDetails(){

    switch (this.typeCase){
      case 'PENSIE':{
        this.router.navigateByUrl('/dash/editPensiiAlimentare/'+this.editDosarID);
        break;
      }
      case 'CIVIL':{
        this.router.navigateByUrl('/dash/dosare-civileCreate/'+this.editDosarID);
        break;
      }
      case 'STAT':{
        this.router.navigateByUrl('/dash/DosareStatCreate/'+this.editDosarID);
        break;
      }
    }
  }

  deleteDosar(){
    var thisref = this;

    this.confirmationService.confirm({
      message: 'Sigur doriți să ștergeți ?',
      header: 'Confirmare',
      icon: 'fa fa-trash',
      accept: () => {
        let postParams = {
          id_login_user: toInt(GlobalVariable.login_id),
          id_case: toInt(thisref.editDosarID)
        };
        var headers = new Headers();
        headers.append("Accept", 'application/json');
        headers.append('Content-Type', 'application/json' );
        let options = new RequestOptions({ headers: headers });

        this.http.post(this.main_url+"/executor/case/delete", postParams , options).subscribe(function (res) {
          console.log(res);
          console.log(postParams)

        });
        setTimeout(()=>{  this.router.navigateByUrl("dash/DosareMele");    },2000);      },
    });
  }
  changeStateCase(){
    var thisref = this;
    var headers = new Headers();
    headers.append("Accept", 'application/json');
    headers.append('Content-Type', 'application/json' );
    let options = new RequestOptions({ headers: headers });

    let postParamsCase = {
      id_login_user : toInt(GlobalVariable.login_id),
      id_case : toInt(thisref.editDosarID),
      select_state : thisref.stateCase
    };
    console.log(postParamsCase)
    this.http.post(this.main_url+"/executor/case/state/change" , postParamsCase ,options ).subscribe(function (res) {
    })

    }
  ngOnInit() {
    var thisref = this;
    jQuery('.card-footer').css({'background-color':'#4a89dc','text-align':'right'});
    jQuery('.ui-fieldset-legend .ui-corner-all .ui-state-default .ui-unselectable-text').css({'background-color': 'cyan'})
    this.route.params.subscribe(function(params){
      thisref.editDosarID = params['name'];
      console.log(thisref.editDosarID);

    });

    var headers = new Headers();
    headers.append("Accept", 'application/json');
    headers.append('Content-Type', 'application/json' );
    let options = new RequestOptions({ headers: headers });

    let postParams1 = {
      id_login_user : toInt(GlobalVariable.login_id),
      id_case : toInt(thisref.editDosarID)
    };
    this.http.post(this.main_url+"/executor/case/details" , postParams1 ,options ).subscribe(function (res) {
      thisref.result_details = res.json();
      console.log(thisref.result_details , 'result_details');
      thisref.case_number = thisref.result_details['case_number'];
      thisref.doc_executor = thisref.result_details['doc_executor'];
      thisref.nom_emitent_instance = thisref.result_details['emitent_instance'];
      thisref.created_date = thisref.result_details['created_date'];
      thisref.expired_date_case = thisref.result_details['expired_date_case'];
      thisref.debitor_credentials = thisref.result_details['debitors'];
      thisref.creditor_credentials = thisref.result_details['creditors'];
      thisref.childrens = thisref.result_details['children'];
      thisref.typeCase = thisref.result_details['type'];
      thisref.stateCase = thisref.result_details['state_case'];

      console.log(thisref.result_details['debitors'] , 'debitor_credentials');



      thisref.file_content = thisref.result_details['files_content'];
    });
  }

}
