import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DetaliiDosarPaComponent } from './detalii-dosar';

describe('DetaliiDosarPaComponent', () => {
  let component: DetaliiDosarPaComponent;
  let fixture: ComponentFixture<DetaliiDosarPaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DetaliiDosarPaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DetaliiDosarPaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
