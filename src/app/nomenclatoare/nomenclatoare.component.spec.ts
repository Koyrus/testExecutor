import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NomenclatoareComponent } from './nomenclatoare.component';

describe('NomenclatoareComponent', () => {
  let component: NomenclatoareComponent;
  let fixture: ComponentFixture<NomenclatoareComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NomenclatoareComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NomenclatoareComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
