import {ChangeDetectorRef, Component, OnInit} from '@angular/core';
import {Eventservice} from "./eventservice";

@Component({
  selector: 'app-eventdata',
  templateUrl: './eventdata.component.html',
  styleUrls: ['./eventdata.component.css']
})

export class EventdataComponent implements OnInit {
  events : any[];
  header : any;
  event : MyEvent;
  dialogVisible : boolean = false;
  idGen : number = 100;
  constructor(private cd: ChangeDetectorRef ){

  }

  handleEventClick(e) {
    this.event = new MyEvent();
    this.event.title = e.calEvent.title;
    console.log("clicked");
    let start = e.calEvent.start;
    let end = e.calEvent.end;
    if(e.view.name === 'month') {
      start.stripTime();
    }

    if(end) {
      end.stripTime();
      this.event.end = end.format();
    }

    this.event.id = e.calEvent.id;
    this.event.start = start.format();
    this.event.allDay = e.calEvent.allDay;
    this.dialogVisible = true;
  }

  ngOnInit(): void {
    this.events = [
      {
        "title": "All Day Event",
        "start": "2017-10-20"
      },
      {
        "title": "Long Event",
        "start": "2017-10-20",
        "end": "2016-10-25"
      },
      {
        "title": "Repeating Event",
        "start": "2016-01-09T16:00:00"
      },
      {
        "title": "Repeating Event",
        "start": "2016-01-16T16:00:00"
      },
      {
        "title": "Conference",
        "start": "2016-01-11",
        "end": "2016-01-13"
      }
    ];
  }
  handleDayClick(event) {
    this.event = new MyEvent();
    this.event.start = event.date.format();
    this.dialogVisible = true;
    console.log("clicked");
    this.cd.detectChanges();
  }
}
export class MyEvent {
  id: number;
  title: string;
  start: string;
  end: string;
  allDay: boolean = true;
}
