import {Component, OnInit} from '@angular/core';
import {Http, RequestOptions , Headers} from "@angular/http";
import {Router} from "@angular/router";
import {GlobalVariable} from "../global";

@Component({
  selector: 'app-newrole',
  templateUrl: './newrole.component.html',
  styleUrls: ['./newrole.component.css']
})
export class NewroleComponent implements OnInit {
  public baseApiUrl = GlobalVariable.globalApiUrl;

  checkboxValue: boolean = false;

  permissions: any[] = ['wasea', 'jora'];
  users : any[] = [];
  role_name : any;
  profiles : { name: string, checked: boolean }[] = [];
  selectedPermissions: any[] = [];
  selectedUsers: any[] = [];
  items: { name: string, checked: boolean }[] = [];
  checkedProfile : any[]=[];
  caseId : any;
  allPermissions : any[]=[];



  constructor(private http: Http, private router: Router) {
  }

  selectAll(checked){
    this.selectedPermissions = [];
    if(checked===true) {
      for (let i = 0; i < this.permissions.length; i++) {
        this.selectedPermissions.push(this.permissions[i].id);
        console.log(this.selectedPermissions , "Selected")

      }
    }
  }




  selectAllUsers(checked){
    this.selectedUsers = [];
    if(checked===true) {
      for (let i = 0; i < this.users.length; i++) {
        this.selectedUsers.push(this.users[i].id);
      }
    }
  }

  ngOnInit() {

    console.log(this.checkboxValue , "Check");
    let perm = localStorage.getItem('permissions');
    this.permissions = JSON.parse(perm)['permissions']['permissions_cases'];
    this.users = JSON.parse(perm)['permissions']['permissions_users'];
    this.profiles = JSON.parse(perm)['profiles'];

    console.log(this.permissions, "Permissions");
    console.log(this.profiles , "Profiles");
    console.log(perm);
  }


  profCredentials = {
    city: null,
    users : this.selectedUsers,
    cases : this.caseId,
    role_name : null,
    is_active : null
  };

  sendRoles(){
    let headers = new Headers();
    headers.append("Accept", 'application/json');
    headers.append('Content-Type', 'application/json' );
    let options = new RequestOptions({ headers: headers });
    this.allPermissions= this.selectedPermissions.concat(this.selectedUsers);
    let postParams = {
      id_login_user : -1,
      role_permissions : this.allPermissions,
      // checked : this.checkedProfile,
      users : this.profCredentials.users,
      cases : JSON.stringify(this.profCredentials.cases),
      role_name : this.role_name,
      is_active : true,
      profile : this.checkedProfile
    };

    this.http.post(this.baseApiUrl+"/executor/admin/roles/save" , postParams , options )
      .subscribe(res =>{
        console.log(res);
        console.log(postParams , "Post params")
      });
    this.router.navigateByUrl('adminDashboard/roles')
  }

}
