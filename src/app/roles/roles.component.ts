import { Component, OnInit } from '@angular/core';
import {Http, RequestOptions , Headers} from "@angular/http";
import {Router} from "@angular/router";
import {ConfirmationService, Message} from "primeng/primeng";
import {GlobalVariable} from "../global";

@Component({
  selector: 'app-roles',
  templateUrl: './roles.component.html',
  styleUrls: ['./roles.component.css']
})
export class RolesComponent implements OnInit {
  public baseApiUrl = GlobalVariable.globalApiUrl;

  constructor(private http:Http , private router : Router , private confirmationService: ConfirmationService) { }
  public results: any[] = [];
  public tableRoles : any[] = [];
  id: null;
  msgs: Message[] = [];
  displayDialog: boolean;

//////////////////////////////////////////
  ////Если роль уже присвоенна - удалить нельзя///////////
  //////////////////////////////

  ngOnInit() {
    var thisref= this;
    this.http.get(this.baseApiUrl+"/executor/admin/roles" , {
    }).subscribe(function (res) {
      thisref.tableRoles = res.json();
      console.log(thisref.tableRoles)
    })
  }

  editRole(id :string){
    var headers = new Headers();
    headers.append("Accept", 'application/json');
    headers.append('Content-Type', 'application/json' );


    let options = new RequestOptions({ headers: headers });
    let postParams = {
      id_login_user : -1,
      id_role: id,
    };

    console.log(postParams);
    this.http.post(this.baseApiUrl+"/executor/admin/roles/create/edit" , postParams , options )
      .subscribe(res =>{
        console.log(res);
        this.router.navigateByUrl('adminDashboard/editRole/'+id);
        var thisref= this;
        thisref.results = res.json();
        console.log(thisref.results);
        localStorage.setItem('data' , JSON.stringify(thisref.results));
        let toArrayUser = thisref.results['set_role_user'];
        let toArrayCases=thisref.results['set_role_case'];
        localStorage.setItem("huinea_user" , toArrayUser);
        localStorage.setItem("huinea_case" , toArrayCases);
      });
  }

  deleteRole(role){
    this.confirmationService.confirm({
      message: 'Do you want to delete this record?',
      header: 'Delete role?',
      icon: 'fa fa-trash',
      accept: () => {
        let postParams = {
          id_login_user: -1,
          id_role: role.id,
          is_delete: true
        };
        console.log(role , "role");
        this.http.post(this.baseApiUrl+"/executor/admin/roles/delete", postParams).subscribe(function (res) {
          console.log(res);

        });
        let index = this.tableRoles.indexOf(role);
        console.log(index, "index");
        this.tableRoles = this.tableRoles.filter((val, i) => i != index);
        this.displayDialog = false;
        this.msgs = [{severity: 'info', summary: 'Confirmed', detail: 'Record deleted'}];
      },
      reject: () => {
        this.msgs = [{severity: 'info', summary: 'Rejected', detail: 'You have rejected'}];
      }
    });
  }
  createRole(){
    this.router.navigateByUrl('adminDashboard/roles/:newrole');
    let headers = new Headers();
    headers.append("Accept", 'application/json');
    headers.append('Content-Type', 'application/json' );

    let options = new RequestOptions({ headers: headers });
    let postParams = {
      id_login_user : -1,
      id_role: null,
    };
    this.http.post(this.baseApiUrl+"/executor/admin/roles/create/edit" , postParams , options )
      .subscribe(res =>{
        console.log(res);
        this.results = res.json();
        console.log(this.results);
        localStorage.setItem('permissions' , JSON.stringify(this.results));
      });
  }
}
