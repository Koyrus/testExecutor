import { Component, OnInit } from '@angular/core';
import {Http, RequestOptions , Headers} from "@angular/http";
import {ActivatedRoute, Router} from "@angular/router";
import {GlobalVariable} from "../global";

@Component({
  selector: 'app-dosare-pensii-edit-steps',
  templateUrl: './dosare-pensii-edit-steps.component.html',
  styleUrls: ['./dosare-pensii-edit-steps.component.css']
})
export class DosarePensiiEditStepsComponent implements OnInit {
  public main_url = GlobalVariable.globalApiUrl;
  public id_dosarPA: number = null;

  constructor(private http : Http , private router : Router ,private route: ActivatedRoute) { }




  ngOnInit() {
    var thisref = this;
    this.route.params.subscribe(function(params){
      thisref.id_dosarPA = parseInt(params['id']);
    });

    var headers = new Headers();
    headers.append("Accept", 'application/json');
    headers.append('Content-Type', 'application/json' );
    let options = new RequestOptions({ headers: headers });
    let postParams = {
      id_login_user : GlobalVariable.login_id,
      id_case_pa : thisref.id_dosarPA
    };

    this.http.post(this.main_url+"/executor/pa/case/create/edit" , postParams ,options ).subscribe(function (res) {
      console.log(res);
    });
  }

}
