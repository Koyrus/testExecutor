import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DosarePensiiEditStepsComponent } from './dosare-pensii-edit-steps.component';

describe('DosarePensiiEditStepsComponent', () => {
  let component: DosarePensiiEditStepsComponent;
  let fixture: ComponentFixture<DosarePensiiEditStepsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DosarePensiiEditStepsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DosarePensiiEditStepsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
