import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {Message} from "primeng/primeng";

@Component({
  selector: 'app-editdosar',
  templateUrl: './editdosar.component.html',
  styleUrls: ['./editdosar.component.css']
})
export class EditdosarComponent implements OnInit {
  public dosarName: string = null;
  constructor(private route: ActivatedRoute , private router : Router) { }


  msgs: Message[];

  uploadedFiles: any[] = [];

  onUpload(event) {
    for(let file of event.files) {
      this.uploadedFiles.push(file);
    }
    console.log(this.uploadedFiles  , "123");
    console.log(event.files , "event.files");

    this.msgs = [];
    this.msgs.push({severity: 'info', summary: 'File Uploaded', detail: ''});
  }

  ngOnInit() {
    let thisref= this;
    this.route.params.subscribe(function(params){
       thisref.dosarName = params['dosarName'];
    })
  }

  onComplete(){
    this.router.navigateByUrl("dash/dosareStat")
  }
}
