import { Component, OnInit } from '@angular/core';
import {Http, RequestOptions , Headers} from "@angular/http";
import {ActivatedRoute, Router} from "@angular/router";
import {split} from "ts-node/dist";
import {GlobalVariable} from "../global";

@Component({
  selector: 'app-edit-roles',
  templateUrl: './edit-roles.component.html',
  styleUrls: ['./edit-roles.component.css']
})
export class EditRolesComponent  implements OnInit {
  localUsers1 = localStorage.getItem("data");
  userslocal = (this.localUsers1)['test_user'];
  test_user = localStorage.getItem("huinea");
  //permissions_ch: any[]=[];

  checkboxValue: boolean = false;
  public id_role: number = null;
  localUsers : any;
  permissions: any[] = ['wasea', 'jora'];
  users : any[] = [];
  role_name : any;
  profiles : { name: string, checked: boolean }[] = [];
  selectedPermissions: any[] = [];
  selectedUsers: any[] = [];
  items: { name: string, checked: boolean }[] = [];
  checkedProfile : any[]=[];
  private _caseId : any;
  allPermissions : any[]=[];
  // test_user :any[]=[];
  public baseApiUrl = GlobalVariable.globalApiUrl;


  constructor(private http : Http , private router : Router ,private route: ActivatedRoute  ) {

    let setUser = localStorage.getItem("huinea_user").split(',').map(function (i) {
      return parseInt(i , 10);
    });
    let setCase = localStorage.getItem("huinea_case").split(',').map(function (i) {
      return parseInt(i , 10);
    });

    this.selectedUsers =setUser;
    this.selectedPermissions=setCase;


  }




  selectAll(checked){
    this.selectedPermissions = [];
    if(checked===true) {
      for (let i = 0; i < this.permissions.length; i++) {
        this.selectedPermissions.push(this.permissions[i].id);
        console.log(this.selectedPermissions , "Selected")

      }
    }
  }


  selectAllUsers(checked){
    this.selectedUsers = [];
    if(checked===true) {
      for (let i = 0; i < this.users.length; i++) {
        this.selectedUsers.push(this.users[i].id);
      }
    }
  }

  ngOnInit() {
    this.localUsers = localStorage.getItem("data");
    let perm = localStorage.getItem('data');
    this.permissions = JSON.parse(perm)['permissions']['permissions_cases'];
    this.users = JSON.parse(perm)['permissions']['permissions_users'];
    this.profiles = JSON.parse(perm)['profiles'];
    let testuser = localStorage.getItem('test_user');
    this.test_user=JSON.parse(testuser);
    let thisref= this;
    this.route.params.subscribe(function(params){
      thisref.id_role = parseInt(params['id']);
    });
  }

  sendEditedRole(id:number){
    let headers = new Headers();
    headers.append("Accept", 'application/json');
    headers.append('Content-Type', 'application/json' );
    let options = new RequestOptions({ headers: headers });
    this.allPermissions= this.selectedPermissions.concat(this.selectedUsers);
    let postParams = {
      id_role: <number>this.id_role,
      id_login_user : -1,
      role_permissions : this.allPermissions,
      role_name : this.role_name,
      is_active : true,
      profile : this.checkedProfile
    };

    this.http.post(this.baseApiUrl+"/executor/admin/roles/save" ,postParams , options )
      .subscribe(res =>{
        console.log(postParams , "PostParams");
        console.log(res , "Response") ;
      })
  }

}
