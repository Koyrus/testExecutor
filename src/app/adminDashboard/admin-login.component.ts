import { Component, OnInit } from '@angular/core';
import {Http, RequestOptions , Headers} from "@angular/http";
import {Router} from "@angular/router";
import {GlobalVariable} from "../global";

@Component({
  selector: 'app-admin-login',
  templateUrl: './admin-login.component.html',
  styleUrls: ['./admin-login.component.css']
})
export class AdminLoginComponent implements OnInit {
  public baseApiUrl = GlobalVariable.globalApiUrl;

  constructor(private http:Http , private router : Router) {

  }

  usersTable(){
    this.router.navigateByUrl('/adminDashboard/users');
    let headers = new Headers();
    headers.append("Accept", 'application/json');
    headers.append('Content-Type', 'application/json' );
    let options = new RequestOptions({ headers: headers });

    let postParams = {
      id_login_user : -1
    };
    this.http.post(this.baseApiUrl+"/executor/admin/users" , postParams , options )
      .subscribe(res =>{
        console.log(res);
        console.log(postParams , "Post params")
      });
  }



// /adminDashboard/users
  ngOnInit() {
  }

}
