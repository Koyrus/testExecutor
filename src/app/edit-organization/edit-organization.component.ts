import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {RequestOptions, Headers, Http} from "@angular/http";
import {GlobalVariable} from "../global";

@Component({
  selector: 'app-edit-organization',
  templateUrl: './edit-organization.component.html',
  styleUrls: ['./edit-organization.component.css']
})
export class EditOrganizationComponent implements OnInit {
  public id: string = null;
  public banks :any[] =[];
  public cities : any = [];
  public codeLK :any;
  public codeName :any;
  public codeEmail :any;
  public codeBankAccout :any;
  public codeIdno : any;


  public selectedDepartment : any[]=[];
  public selectedCity : any[] = [];


  constructor(private router : Router , private route: ActivatedRoute , private http:Http) { }
  public baseApiUrl = GlobalVariable.globalApiUrl;

  ngOnInit() {
    let thisref= this;
    this.route.params.subscribe(function(params){
      thisref.id = params['id'];
    });

    let ban = localStorage.getItem('data');
    this.banks = JSON.parse(ban)['nomenclator']['banks_nom'];
    this.cities = JSON.parse(ban)['nomenclator']['cities_nom'];

    this.selectedDepartment = JSON.parse(ban)['department'];
    this.selectedCity = JSON.parse(ban)['department'];

    console.log(this.selectedDepartment , "1231  3212312312312");
    console.log(this.banks);
    console.log(this.cities, "city");
    this.dateCredentials.code=this.selectedDepartment['licence_code'];
    this.dateCredentials.idno=this.selectedDepartment['idno'];
    this.dateCredentials.name=this.selectedDepartment['name'];
    this.dateCredentials.email=this.selectedDepartment['email'];
    this.dateCredentials.post_code=this.selectedDepartment['post_code'];

  }

  dateCredentials = {
    code: this.codeLK,
    idno: this.codeIdno,
    email: this.codeEmail,
    name : this.codeName,
    city: null,
    addressa: null ,
    post_code: null ,
    hoursw: null ,
    bank: null ,
    id : null,
    login_user_id : null,
    street : null,
    block : null,
    bank_name_id : this.codeBankAccout
  };

  public editOrganization(){
    var headers = new Headers();
    headers.append("Accept", 'application/json');
    headers.append('Content-Type', 'application/json' );
    let options = new RequestOptions({ headers: headers });
    let thisref= this;
    this.route.params.subscribe(function(params){
      thisref.id = params['id'];
    });
    let postParams = {
      id : thisref.id,
      id_login_user : -1,
      licence_code: this.dateCredentials.code,
      name:"name",
       city:6,
      idno : this.dateCredentials.idno,
      email : this.dateCredentials.email,
      // city : this.dateCredentials.city,
      post_code : this.dateCredentials.post_code,
      // hoursw : this.dateCredentials.hoursw,
      bank_acount : this.dateCredentials.bank,
       // bank_name_id : this.dateCredentials.bank,
        bank_name_id : 2
    };
    console.log(postParams);
    this.http.post(this.baseApiUrl+"/executor/admin/organization/save" , postParams , options ).subscribe(res =>{
      console.log(res);
    });
    this.router.navigateByUrl("adminDashboard/organization");
    localStorage.clear();
  }
}
