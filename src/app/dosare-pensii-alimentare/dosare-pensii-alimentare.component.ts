import { Component, OnInit } from '@angular/core';
import {Http, RequestOptions ,Headers} from "@angular/http";
import {Router} from "@angular/router";
import {GlobalVariable} from "../global"
import {EditDosarePensiiAlimentareComponent} from "../edit-dosare-pensii-alimentare/edit-dosare-pensii-alimentare.component";
import {ConfirmationService, Message} from "primeng/primeng";
import {toInt} from "ngx-bootstrap/bs-moment/utils/type-checks";

@Component({
  selector: 'app-dosare-pensii-alimentare',
  templateUrl: './dosare-pensii-alimentare.component.html',
  styleUrls: ['./dosare-pensii-alimentare.component.css']
})

export class DosarePensiiAlimentareComponent implements OnInit {
  msgs: Message[] = [];
  public baseApiUrl = GlobalVariable.globalApiUrl;
  public usersPa : any[]=[];
  public userrs : any[]=[];
  public id_dosar : string = 'new';
  public pensiiAlimentare : any[];
  display: boolean =false;
  private currentPage: number = 1;
  displayDialog: boolean;

  public per_page: number[] = [
    10,
    20,
    30
  ];
  total_records: number = 0;



  constructor(private http:Http, private router:Router,private confirmationService: ConfirmationService) {

    var thisref = this;
    this.usersPa=[];
    var headers = new Headers();
    headers.append("Accept", 'application/json');
    headers.append('Content-Type', 'application/json' );
    headers.append('Cache-Control', 'no-cache');

    let options = new RequestOptions({ headers: headers });
    let postParams = {
      id_login_user: toInt(GlobalVariable.login_id),
    };
    console.log(this.usersPa,'Init');

    this.http.post(this.baseApiUrl+"/executor/pa/case/all" , postParams , options).subscribe(function (res) {
      // thisref.users = res.json();
      console.log(res.json(), "Users");

      var gg={"id":1};
     // GlobalVariable.gl="tre";
     // GlobalVariable.globalTest=res.json();
       thisref.usersPa=res.json();
       console.log(thisref.usersPa , 'usersPa')
    });

    console.log(this.usersPa,'ssssssssssss');
    console.log('ngOnInit Case pa');

  }

   ngOnInit() {

  }

  deleteDosar(dosar){
    this.confirmationService.confirm({
      message: 'Sigur doriți să ștergeți ?',
      header: 'Confirmare',
      icon: 'fa fa-trash',
      accept: () => {
        let postParams = {
          id_login_user: toInt(GlobalVariable.login_id),
          id_case: dosar.id_case
        };
        var headers = new Headers();
        headers.append("Accept", 'application/json');
        headers.append('Content-Type', 'application/json' );
        let options = new RequestOptions({ headers: headers });

        this.http.post(this.baseApiUrl+"/executor/case/delete", postParams , options).subscribe(function (res) {
          console.log(res);
          console.log(postParams)

        });
        let index = this.usersPa.indexOf(dosar);
        console.log(index, "index");
        this.usersPa = this.usersPa.filter((val, i) => i != index);
        this.displayDialog = false;
        this.msgs = [{severity: 'success', summary: 'Confirmarea', detail: 'Succes'}];
      },
      reject: () => {
        // this.msgs = [{severity: 'info', summary: 'Rejected', detail: 'You have rejected'}];
      }
    });
  }
  detaliiDosar(id : number){
    this.router.navigateByUrl('/dash/detalii-dosar/'+id)
  }

  editSteps(id : number){
    this.router.navigateByUrl('/dash/editPensiiAlimentare/'+id);
    console.log(id);
  }

  createOrganization(){

    this.router.navigateByUrl('/dash/editPensiiAlimentare/:'+this.id_dosar);

  }

}
