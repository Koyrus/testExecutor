import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DosarePensiiAlimentareComponent } from './dosare-pensii-alimentare.component';

describe('DosarePensiiAlimentareComponent', () => {
  let component: DosarePensiiAlimentareComponent;
  let fixture: ComponentFixture<DosarePensiiAlimentareComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DosarePensiiAlimentareComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DosarePensiiAlimentareComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
