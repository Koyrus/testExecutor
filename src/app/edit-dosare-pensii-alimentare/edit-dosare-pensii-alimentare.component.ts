import {AfterViewInit, Component, ElementRef, Input, OnInit, Renderer2, VERSION, ViewChild} from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {FileUpload, Message} from "primeng/primeng";
import {Http, RequestOptions , Headers} from "@angular/http";
import {GlobalVariable} from "../global";
import {toInt} from "ngx-bootstrap/bs-moment/utils/type-checks";


@Component({
  selector: 'app-edit-dosare-pensii-alimentare',
  templateUrl: './edit-dosare-pensii-alimentare.component.html',
  styleUrls: ['./edit-dosare-pensii-alimentare.component.css'],


})


export class EditDosarePensiiAlimentareComponent implements OnInit ,AfterViewInit  {
  @ViewChild('fileInput') el:ElementRef;
  @ViewChild("cerere_file") cerere_file: FileUpload;
  @Input()case_id_edit :number = 0;
  @Input() doneBtnText='123';
  @Input() nextBtnText='345';
  @Input() previousBtnText='567';
  ngAfterViewInit(): void {
    jQuery('.card-footer').css('background-color','#4a89dc');
    jQuery('.card-footer').css('text-align','right');


    var buttonPrev = jQuery('.card-footer button:first').text('Precedent');
    var buttonNext = jQuery('.card-footer button:first').next().text('Următorul');
    var buttonPrev2 = jQuery('.card-footer button:last').text('Salvează');





    jQuery('.ui-inputtext').css({
      'border':'solid 1px',
      'border-color':'#999999',
      'padding-left':'5px',
      'height':'2rem'
    });



  }
public case_team : any[]=[];
  public current_date : any;

  constructor(private http : Http , private router : Router , private route : ActivatedRoute) {

    // var currentYear = (new Date).getFullYear();
    // console.log(currentYear);


    var currentTime = new Date()
    var month = currentTime.getMonth() + 1
    var day = currentTime.getDate()
    var year = currentTime.getFullYear()
    var dateString=month + "/" + day + "/" + year
    this.current_date = dateString;
    console.log(dateString , ' **********')

    var thisref = this;
    console.log(this.case_id_edit , 'case_id_edit');
    this.route.params.subscribe(function(params){
      thisref.editDosarID = params['name'];
      console.log(thisref.editDosarID);
    });

    var headers = new Headers();
    headers.append("Accept", 'application/json');
    headers.append('Content-Type', 'application/json' );
    let options = new RequestOptions({ headers: headers });

    let postParams = {
      id_login_user: toInt(GlobalVariable.login_id),
      id_case_pa : null
    };

    if(thisref.editDosarID > 0){
      postParams.id_case_pa = toInt(thisref.editDosarID)
    }else {
      postParams.id_case_pa = null;
    }


    this.http.post(this.main_url+"/executor/pa/case/create/edit" , postParams ,options ).subscribe(function (res) {
      console.log(res);
      thisref.case_numberPA = res.json()['case_number'];
      thisref.case_team = res.json()['case_team'];

      thisref.fillData(res.json(),thisref);

    });

  }


  selectedEntry1;
  public case_numberPA : string;
  // public baseApiUrl = GlobalVariable.globalApiUrl;
  public state_case : string;
  //ID кейса///
  public editDosarID : number = null;
  public state_dosar :any[]=[];
    public id : number;
  public myText: string = '';
  public myTextCreditor : string = '';
  public myTextChilds : string = '';
  msgs: Message[];
  public disable_file : boolean = true;
  public childs : any[]=[];
  public dosarName: string = null;
  public childrens : any[]=[];

  public display : boolean=false;
  public pustoi_array : string;
  public index : number = 1;
  public pensiiAlNumber : any;
  public stareDosarPA : string;
  public file_name : any;
  public payment_mode : any[] = [];
  public priority_mode : any[] = [];
  public arrayForAutocompleteDebitor : any[]=[];
  public arrayForChilds : any[] = [];
  public debitor_Test :string;
  public id_file_type : number = 1;
  public url_for_upload = 'http://192.168.1.69:8080/user/case/fileupload/' + this.id_file_type;
  public main_url = GlobalVariable.globalApiUrl;

  //FileUpload!!!!
  public choose_file_type : any[]=[];
  public url: string = 'http://192.168.1.69:8080/user/case/fileupload';
  @Input() multiple: boolean = false;
  @ViewChild('fileInput') inputEl: ElementRef;
  public uploadCaseFiles : File[]=[];
  public uploadedContent : any[]=[];
  /////////////////////////////////////////
  doc_executor : any;
  inst_emit_id : any;
  /////////////////Creditor///////////////////
  public id_creditor : number;
  public idnp_creditor : any;
  public id_city_creditor : number = 1;
  public first_name_creditor : string;
  public last_name_creditor : string;
  public patronimic_creditor : string;
  public birth_date_creditor : Date;
  public post_code_creditor : string;
  public city_creditor : string;
  public street_creditor : string;
  public block_creditor : string;
  /////////////////Debitor///////////////////
  public id_debitor : number;
  public idnp_debitor : any;
  public id_city_debitor : number = 1;
  public first_name_debitor : string;
  public last_name_debitor : string;
  public patronimic_debitor : string;
  public birth_date_debitor : Date;
  public post_code_debitor : string;
  public city_debitor : string;
  public street_debitor : string;
  public block_debitor : string;

  public validIDNp:boolean=false;
  public stateType : boolean = true;

  date_from;
  date_to;
  end_date_case;
  date;
  public emitent_instance : any[]=[];
  public payment_instance : any[]=[];
  public priority_instance : any[] = [];
  document_type : any;
  sum : any;
  text_procedura : any;
  stateCase: string = 'Initiere';
  public emitent_resultAutoComplete : string[]=[];
  text: string;
  filteredBrands: any[];
  public selectedPriorityEntry;
  public selectedPaymentEntry;
  onSelectionChangePriority(priority){
    console.log(priority,'+++++++++++++++++++++++++++++++++++++++');
    this.selectedPriorityEntry = priority;
    this.selectedPriorityID = priority.id;

  }
  onSelectionPaymentChange(item){
    console.log(item , '-----------------------------------------');
    this.selectedPaymentEntry = item;
    this.selectedPayModeID = item.id;
  }
  filterBrands(event) {
    this.filteredBrands = null;
    var thisref = this;
    if(this.emitent_instance.length > 1){
      this.http.get(this.main_url+'/executor/case/instance/search/'+this.emitent_instance).subscribe(function (res) {
        thisref.emitent_resultAutoComplete = res.json();
        console.log(thisref.emitent_resultAutoComplete);
      });
    }
  }

  myFuncDebitorIDNP(){
    this.myText = this.idnp_debitor.length;


    if(this.idnp_debitor.length == 13){
      this.validIDNp = true;

      var thisref= this;
      setTimeout(()=>{
        var headers = new Headers();
        headers.append("Accept", 'application/json');
        headers.append('Content-Type', 'application/json' );
        let options = new RequestOptions({ headers: headers });

        let postData = {
          idnp : toInt(this.idnp_debitor),
          id_login_user: toInt(GlobalVariable.login_id),
        };


        this.http.post(this.main_url+'/executor/case/idnp/search' , postData , options).subscribe(function (res) {
          thisref.arrayForAutocompleteDebitor = res.json();
          if(thisref.arrayForAutocompleteDebitor['id'] != null) {
            thisref.arrayForAutocompleteDebitor = res.json();
            thisref.id_debitor = thisref.arrayForAutocompleteDebitor['id'];
            thisref.idnp_debitor = thisref.arrayForAutocompleteDebitor['idnp'];
          //  thisref.id_city_debitor = thisref.arrayForAutocompleteDebitor['id_city'];
            thisref.id_city_debitor = 1;
            thisref.first_name_debitor = thisref.arrayForAutocompleteDebitor['first_name'];
            thisref.last_name_debitor = thisref.arrayForAutocompleteDebitor['last_name'];
            thisref.patronimic_debitor = thisref.arrayForAutocompleteDebitor['patronimic'];
            thisref.birth_date_debitor = thisref.arrayForAutocompleteDebitor['birth_date'];
            thisref.post_code_debitor = thisref.arrayForAutocompleteDebitor['post_code'];
            thisref.city_debitor = thisref.arrayForAutocompleteDebitor['city'];
            thisref.street_debitor = thisref.arrayForAutocompleteDebitor['street'];
            thisref.block_debitor = thisref.arrayForAutocompleteDebitor['block'];
            console.log('autocomplete off');

          }else {
            thisref.id_debitor = null;
            thisref.stateType = false;

          }
        })},1500);
        }else {

      if (this.validIDNp) {
        this.clearAllfieldsDebitor(this.index);
        this.validIDNp = false
      }
    }
  }


  nextAchitari(){
    var thisref = this;
    for(let i = 0;i<this.childrens.length;i++){
      let birth_date_temp = this.childrens[i]['birth_date'];
      if(birth_date_temp != ''){
        var date = new Date(birth_date_temp);
        var date1 = date.getFullYear()+18;
        date.setFullYear(date1);
        console.log(date ,'date');
        console.log(birth_date_temp ,'dataTest');
        console.log(date1,'date1');
        // this.childrens[i]['majority_date'] = date.toLocaleDateString('en-GB');
      }

    }
  }


  myFuncChildrensIDNP(){
    var thisref = this;

    let childData=thisref.childrens[this.index-1];

    let idnp_dat=childData.idnp;
    thisref.myTextChilds = idnp_dat.length;

    if(thisref.childrens[thisref.index-1].idnp.length == 13){
      setTimeout(()=>{
        var headers = new Headers();
        headers.append("Accept", 'application/json');
        headers.append('Content-Type', 'application/json' );
        let options = new RequestOptions({ headers: headers });

        let postData = {
          idnp : toInt(idnp_dat),
          id_login_user: toInt(GlobalVariable.login_id),
        };


        this.http.post(this.main_url+'/executor/case/idnp/search' , postData , options).subscribe(function (res) {
          thisref.arrayForChilds = res.json();
          if(thisref.arrayForChilds['id'] != null && thisref.arrayForChilds['id'] != ''){
            childData['last_name']=thisref.arrayForChilds['last_name'];
            childData['idnp']=thisref.arrayForChilds['idnp'];
            childData['first_name']=thisref.arrayForChilds['first_name'];
            childData['birth_date']=thisref.arrayForChilds['birth_date'];
            childData['id']=thisref.arrayForChilds['id'];


          }
        })},1500);
    }
  }

  myFuncCreditorIDNP() {
    this.myTextCreditor = this.idnp_creditor.length;


    if (this.idnp_creditor.length == 13) {
      this.validIDNp = true;
      var thisref = this;
      setTimeout(() => {
        var headers = new Headers();
        headers.append("Accept", 'application/json');
        headers.append('Content-Type', 'application/json');
        let options = new RequestOptions({headers: headers});

        let postData = {
          idnp: toInt(this.idnp_creditor),
          id_login_user: toInt(GlobalVariable.login_id),
        };


        this.http.post(this.main_url + '/executor/case/idnp/search', postData, options).subscribe(function (res) {
          thisref.arrayForAutocompleteDebitor = res.json();
          if (thisref.arrayForAutocompleteDebitor['id'] != null && thisref.arrayForAutocompleteDebitor['id'] != '') {
            thisref.id_creditor = thisref.arrayForAutocompleteDebitor['id'];
            thisref.idnp_creditor = thisref.arrayForAutocompleteDebitor['idnp'];
            // thisref.id_city_creditor = thisref.arrayForAutocompleteDebitor['id_city'];
            thisref.id_city_creditor = 1;
            thisref.first_name_creditor = thisref.arrayForAutocompleteDebitor['first_name'];
            thisref.last_name_creditor = thisref.arrayForAutocompleteDebitor['last_name'];
            thisref.patronimic_creditor = thisref.arrayForAutocompleteDebitor['patronimic'];
            thisref.birth_date_creditor = thisref.arrayForAutocompleteDebitor['birth_date'];
            thisref.post_code_creditor = thisref.arrayForAutocompleteDebitor['post_code'];
            thisref.city_creditor = thisref.arrayForAutocompleteDebitor['city'];
            thisref.street_creditor = thisref.arrayForAutocompleteDebitor['street'];
            thisref.block_creditor = thisref.arrayForAutocompleteDebitor['block'];
            thisref.stateType = true;

          } else {
            thisref.id_creditor = null;
            thisref.stateType = false;

          }


        })
      }, 1500);
    } else {

      if (this.validIDNp) {
        this.clearAllfields(this.index);
        this.validIDNp = false
      }
    }

  }

  clearAllfields(index:number){
    var thisref = this;
    thisref.id_creditor=null ;
    thisref.id_city_creditor =null;
    thisref.first_name_creditor =null;
    thisref.last_name_creditor=null;
    thisref.patronimic_creditor=null;
    thisref.birth_date_creditor=null;
    thisref.post_code_creditor=null;
    thisref.city_creditor=null;
    thisref.street_creditor=null;
    thisref.block_creditor=null;

    ////////////////////////////


  }
  clearAllfieldsDebitor(index:number){
    var thisref = this;

    thisref.id_debitor=null ;
    thisref.id_city_debitor =null;
    thisref.first_name_debitor =null;
    thisref.last_name_debitor=null;
    thisref.patronimic_debitor=null;
    thisref.birth_date_debitor=null;
    thisref.post_code_debitor=null;
    thisref.city_debitor=null;
    thisref.street_debitor=null;
    thisref.block_debitor=null;
  }



  achitari(){
  }

  onSelectFile(){

  let tempPostTypeDocs = {
      'id_document' : null,
      'file_name' : this.cerere_file.files[0].name,
      'doc_type' : this.choose_file_type['name'],
      'id_doc_type' : this.choose_file_type['id'],
      'is_delete' : false
    };
    this.uploadedContent.push(tempPostTypeDocs);
    this.uploadCaseFiles.push(this.cerere_file.files[0]);
    this.cerere_file.clear();
  }

  deleteCaseFileFromArray(file_select : any){
   //
  //


    if(file_select.id_document!=null && file_select.id_document !=''){
      for(var y=0;y< this.uploadedContent.length;y++){
        let file_elem=this.uploadedContent[y];
        if(file_elem.id_document === file_select.id_document){
          file_elem.is_delete=true;
        }
      }
    }else{

     // this.uploadCaseFiles = this.uploadCaseFiles.filter((val, i) => val.name != file_select.file_name);
      this.uploadedContent = this.uploadedContent.filter((val, i) => val.file_name != file_select.file_name && file_select.id_document == null);
    }






    for(var y=0;y< this.uploadedContent.length;y++){

    }




    // for(let i =0 ; i <this.uploadCaseFiles.length;i++){}
   // console.log(this.uploadCaseFiles.length,'list Files');




  }


  selectedTeamEntry;
  selectedTeamID : number = null;
  public array_team_members : any[]=[];
  public unique : any[]=[];
  static onlyUnique(value, index, self) {
    return self.indexOf(value) === index;
  }

  changeTeamMembers(team){
    this.selectedTeamEntry = team;
    this.selectedTeamID = team.id;
    this.selectedTeamEntry['is_selected'] = true;
    this.array_team_members.push(this.selectedTeamEntry);
    for(let i = 0;i <this.array_team_members.length;i++ ){
      var a = this.array_team_members;
      this.unique = a.filter( EditDosarePensiiAlimentareComponent.onlyUnique );
    }
  }


    onChangeFunction(){
      if (this.choose_file_type != null){
        this.disable_file = false;
      }
  }

  ngOnInit() {


    let perm = localStorage.getItem('pa');
    // this.pensiiAlNumber = JSON.parse(perm)['case_number'];
    // this.stareDosarPA = JSON.parse(perm)['stateCase'];
    this.date = new Date();

    var thisref = this;
    this.http.get(this.main_url+'/executor/case/nomenclator/paymode' ,{}).subscribe(function (response) {
      thisref.payment_mode = response.json();

    });

    this.http.get(this.main_url+'/executor/case/nomenclator/priority').subscribe(function (res) {
      thisref.priority_mode = res.json()
    });

    this.http.get(this.main_url+'/executor/case/nomenclator/doctype').subscribe(function (res) {
      thisref.document_type = res.json();
      thisref.choose_file_type = thisref.document_type[0];

    });

    this.route.params.subscribe(function(params){
      thisref.dosarName = params['dosarName'];
    });
    for (let i = 1; i <10; i++) {
      var text = { 'order_id':i,'first_name':'','last_name' : '' ,'idnp': '' , 'birth_date': '' };
       this.childrens.push(text);
    }
    this.pustoi_array = this.childrens[0].name;
  }

  onBasicUpload() {}
  onStep1Next(){


  }
  onStepDebitor(){

  }
  onStepCreditor(){
    this.index=1;
  }

  onComplete(){
    var thisref = this;
    var headers = new Headers();
    headers.append("Accept", 'application/json');
    headers.append('Content-Type', 'application/json' );
    let options = new RequestOptions({ headers: headers });

    for (let child of this.childrens){
      if(child.first_name != ""){
        // child.id = null;
        // child.birth_date = child.birth_date;
        this.childs.push(child);

      }
    }


    let postParams = {
      id : this.id,
      id_login_user: toInt(GlobalVariable.login_id),
      case_number : this.case_numberPA,
      ////Step1///
      stareDosar : this.state_case,
      nom_emitent_instance : this.emitent_instance,
      nom_pay_mode : this.selectedPaymentEntry,
      nom_priority : this.selectedPriorityEntry,
      doc_executor : this.doc_executor,
      ////Step2///
      debitor : {
        id : this.id_debitor,
        idnp : this.idnp_debitor,
        // id_city : this.id_city_debitor,
         id_city : 1,
        first_name : this.first_name_debitor,
        last_name : this.last_name_debitor,
        patronimic : this.patronimic_debitor,
        birth_date :  this.birth_date_debitor,
        city : this.city_debitor,
        street:this.street_debitor,
        block:this.block_debitor,
        post_code : this.post_code_debitor
      },
      ////Step3///
      creditor : {
        id : this.id_creditor,
        idnp : this.idnp_creditor,
        // id_city : this.id_city_creditor,
        id_city : 1,
        first_name : this.first_name_creditor,
        last_name : this.last_name_creditor,
        patronimic : this.patronimic_creditor,
        birth_date :  this.birth_date_creditor,
        city : this.city_creditor,
        street:this.street_creditor,
        block:this.block_creditor,
        post_code : this.post_code_creditor
      },
      ////Step4///
      children: this.childs,
      ////Step5///
      date_from : this.date_from,
      amount : parseFloat(this.sum),
      ////Step6///
      expired_date_case : this.end_date_case,
      doc_type : this.choose_file_type,
      text_procedure : this.text_procedura,
      case_team: this.unique,
      files_content : this.uploadedContent
    };
    console.log(postParams , 'idnp');
    this.sendDatePAcase(postParams);




  }
  sendDatePAcase(stepsData : any): void {
    var headers = new Headers();
    let options = new RequestOptions({ headers: headers });
    var form = new FormData();
    for(let i = 0;i < this.uploadCaseFiles.length ; i++){
      form.append("file",  this.uploadCaseFiles[i]);
    }
    form.append('case_pa', new Blob([JSON.stringify(stepsData)], {
      type: "application/json"
    }));
    this.http.post(this.main_url+'/executor/pa/case/save' , form, {
      headers: headers
    }).subscribe(function (res) {
      console.log(res , "res");

    });
    setTimeout(()=>{  this.router.navigateByUrl("dash/dosarePensiiAlimentare");    },3000);

    console.log(form , 'form send PA Case');

  }




  // sendDatePAcase(stepsData : any):Observable<boolean> {
  //
  //   var headers = new Headers();
  //   let options = new RequestOptions({ headers: headers });
  //   var form = new FormData();
  //   for(let i = 0;i < this.uploadCaseFiles.length ; i++){
  //     form.append("file",  this.uploadCaseFiles[i]);
  //   }
  //   console.log(this.uploadCaseFiles.length , "Length PA file");
  //   form.append('case_pa', new Blob([JSON.stringify(stepsData)], {
  //     type: "application/json"
  //   }));
  //   let savePA = false;
  //
  //  return this.http.post(this.main_url+'/executor/pa/case/save' , form, options).map(
  //    res => {
  //      if(res.status == 200){
  //        return true;
  //      } else return false;
  //    } );
  //
  //
  // }


public setInst : any[]=[];
public selectedPayModeID : number;
public selectedPriorityID : number;
public created_PA_date : string;
public child_array : any[]=[];
  fillData(casePAEdit:any[], thisref:any){
    // let currentTime = Date.now();
    // console.log(currentTime);

    console.log(this.current_date , 'current_datecurrent_datecurrent_date');
    thisref.state_case = casePAEdit['state_case'];

    thisref.created_PA_date = casePAEdit['created_date']
   if(casePAEdit['id']>0) {

     thisref.id = casePAEdit['id'];
      ////First page////
     thisref.uploadedContent = casePAEdit['files_content'];
     console.log(thisref.uploadedContent , 'thisref.uploadCaseFiles');
     thisref.emitent_instance = casePAEdit['nom_emitent_instance'];
     thisref.doc_executor = casePAEdit['doc_executor'];
     ///Debitor Page///
     thisref.id_debitor = casePAEdit['debitor']['id'];
     thisref.idnp_debitor = casePAEdit['debitor']['idnp'];
     thisref.first_name_debitor = casePAEdit['debitor']['first_name'];
     thisref.last_name_debitor = casePAEdit['debitor']['last_name'];
     thisref.patronimic_debitor = casePAEdit['debitor']['patronimic'];
     thisref.city_debitor = casePAEdit['debitor']['city'];
     thisref.street_debitor = casePAEdit['debitor']['street'];
     thisref.block_debitor = casePAEdit['debitor']['block'];
     thisref.birth_date_debitor = casePAEdit['debitor']['birth_date'];
     thisref.post_code_debitor = casePAEdit['debitor']['post_code'];
     ///Creditor/////
     thisref.id_creditor = casePAEdit['creditor']['id'];
     thisref.idnp_creditor = casePAEdit['creditor']['idnp'];
     thisref.first_name_creditor = casePAEdit['creditor']['first_name'];
     thisref.last_name_creditor = casePAEdit['creditor']['last_name'];
     thisref.patronimic_creditor = casePAEdit['creditor']['patronimic'];
     thisref.city_creditor = casePAEdit['creditor']['city'];
     thisref.street_creditor = casePAEdit['creditor']['street'];
     thisref.block_creditor = casePAEdit['creditor']['block'];
     thisref.birth_date_creditor = casePAEdit['creditor']['birth_date'];
     thisref.post_code_creditor = casePAEdit['creditor']['post_code'];
     thisref.childrens = casePAEdit['children'];
      // console.log(thisref.childrens , 'childrens');
      thisref.child_array.push(thisref.childrens);
      console.log(thisref.child_array ,'**************')
     thisref.validIDNp=true;
     let totalChildren = thisref.childrens.length;

     for(var m=0; m < totalChildren; m++){
       thisref.childrens[m]['order_id'] = m+1;
       console.log(thisref.childrens[0]['order_id'],'ssssssssssssssssssssssssssssssssssssssssssssssssss')
     }
for(var k=1; k < 7; k++){
  var text = {'order_id':totalChildren+k,'first_name':'','last_name' : '' ,'idnp': '' , 'birth_date': '' };
  thisref.childrens.push(text);
}
  /////////Achitari////
  //    thisref.setInst = casePAEdit['nom_pay_mode']['name'];
     thisref.selectedPayModeID = casePAEdit['nom_pay_mode']['id'];
     thisref.selectedPaymentEntry = casePAEdit['nom_pay_mode'];
     thisref.date_from = casePAEdit['date_from'];
     thisref.date_to = casePAEdit['date_to'];
     thisref.sum = casePAEdit['amount'];
     ////////////Prioritate/////////////
     thisref.selectedPriorityEntry = casePAEdit['nom_priority'];
     thisref.selectedPriorityID = casePAEdit['nom_priority']['id'];
     thisref.end_date_case = casePAEdit['expired_date_case'];
     thisref.text_procedura = casePAEdit['text_procedure'];
   // public selectedPriorityEntry;
   // public selectedPaymentEntry;
   }
  }
}
