import { Component, OnInit } from '@angular/core';
import {Http, RequestOptions , Headers} from "@angular/http";
import {ActivatedRoute, Router} from "@angular/router";
import {ConfirmationService, Message} from "primeng/primeng";
import {GlobalVariable} from "../global";

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit {
  public baseApiUrl = GlobalVariable.globalApiUrl;

  public users : any[] = [];
  public results : any[] = [];
  public total_recors:number=0;
  id: null;
  msgs: Message[] = [];
  displayDialog: boolean;
  public userByGetRequest : any[] = [];
  constructor(private http : Http , private router : Router , private route: ActivatedRoute,private confirmationService: ConfirmationService ) { }




  createUser(){
    this.router.navigateByUrl("adminDashboard/newuser");
    let headers = new Headers();
    headers.append("Accept", 'application/json');
    headers.append('Content-Type', 'application/json' );
    let options = new RequestOptions({ headers: headers });

    let postParams = {
      id_login_user : -1
    };
    this.http.post(this.baseApiUrl+"/executor/admin/users/create/edit" , postParams , options )
      .subscribe(res =>{
        console.log(res);
        console.log(postParams , "Post params");
        this.users = res.json();
        localStorage.setItem('users' , JSON.stringify(this.users));
      });
  }


  editUser(id :string){
    let headers = new Headers();
    headers.append("Accept", 'application/json');
    headers.append('Content-Type', 'application/json' );
    let options = new RequestOptions({ headers: headers });

    let postParams = {
      id_login_user : -1,
      id_user: id
    };

    this.http.post(this.baseApiUrl+"/executor/admin/users/create/edit" , postParams , options )
      .subscribe(res =>{
        const thisref = this;
        console.log(res);
        // console.log(postParams , "Post params");
        thisref.results = res.json();
        localStorage.setItem("editUsers" , JSON.stringify(res));
        this.router.navigateByUrl("adminDashboard/editUser/" +id);

      });

  }

  deleteUser(user){
    this.confirmationService.confirm({
      message: 'Do you want to delete this record?',
      header: 'Delete Confirmation?',
      icon: 'fa fa-trash',
      accept: () => {
        let postParams = {
          id_login_user: -1,
          id_user: user.id_user,
          is_delete: true
        };
        console.log(user , "user")
        this.http.post(this.baseApiUrl+"/executor/admin/users/delete", postParams).subscribe(function (res) {
          console.log(res);

        });
        let index = this.results.indexOf(user);
        console.log(index, "index");
        this.results = this.results.filter((val, i) => i != index);
        this.displayDialog = false;
        this.msgs = [{severity: 'info', summary: 'Confirmed', detail: 'Record deleted'}];
      },
      reject: () => {
        this.msgs = [{severity: 'info', summary: 'Rejected', detail: 'You have rejected'}];
      }
    });
  }


  activatedUser(user){
    console.log(user, "user");

    let postParams = {
      id_login_user: -1,
      id_user: user.id_user,
      is_active: user.is_active
    };
    console.log(!user.is_active, "!organization.is_active");
    this.http.post(this.baseApiUrl+"/executor/admin/users/active", postParams).subscribe(function (res) {

      console.log(res);


    });
  }

  ngOnInit() {
    var thisref= this;
    this.http.get(this.baseApiUrl+'/executor/admin/users' , {
    }).subscribe(function (res) {
      thisref.results = res.json();
      thisref.total_recors = thisref.results.length;
      console.log(thisref.results);
    })

  }

}
