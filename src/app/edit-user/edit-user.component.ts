import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {Http, RequestOptions , Headers} from "@angular/http";
import {GlobalVariable} from "../global";

@Component({
  selector: 'app-edit-user',
  templateUrl: './edit-user.component.html',
  styleUrls: ['./edit-user.component.css']
})
export class EditUserComponent implements OnInit {
  public baseApiUrl = GlobalVariable.globalApiUrl;

  public id: number = null;
  public roles :any[] =[];
  public users : any;
  public rolesFor : any[] = [];
  public userOrganizations : any = [];
  public userWorkFunctions : any = [];
  public selectedDepartment : any[] = [];
  public roleUser : any[]=[];
  usernameRL : any;
  passwordRL : any;
  emailRL : any;
  phoneRL : any;
  idnpRL : any;
  firstnameRL : any;
  lastnameRL : any;
  constructor(private http : Http , private router : Router, private route: ActivatedRoute) {}

  isEmployee(roleUsr){
    if(this.roleUser != null){
      return this.roleUser['is_employe'];
    }else if(roleUsr != null){
      return roleUsr.is_employe;
    }
  }
  submitToServer(){
    let formData = new FormData();
    let headers = new Headers();
    headers.append("Accept", 'application/json');
    headers.append('Content-Type', 'application/json' );
    let options = new RequestOptions({ headers: headers });

    this.http.post("asdasdasd", formData );
  }
  userCredentials = {
    username: this.usernameRL,
    password : this.passwordRL ,
    email : this.emailRL,
    idnp : this.idnpRL,
    phone : this.phoneRL,
    firstname : this.firstnameRL,
    lastname : this.lastnameRL

  };
  ngOnInit() {
    let thisref= this;
    this.route.params.subscribe(function(params){
      thisref.id = params['id'];
    });
    let perm = localStorage.getItem('editUsers');
    this.users = JSON.parse(perm)['_body'];
    this.rolesFor = JSON.parse(this.users)['roles'];
    this.userOrganizations = JSON.parse(this.users)['organizations'];
    this.userWorkFunctions = JSON.parse(this.users)['work_functions'];
    this.roleUser = JSON.parse(this.users)['role'];
    this.userCredentials.username = JSON.parse(this.users)['username'];
    this.userCredentials.password = JSON.parse(this.users)['password'];
    this.userCredentials.email = JSON.parse(this.users)['email'];
    this.userCredentials.idnp = JSON.parse(this.users)['idnp'];
    this.userCredentials.phone = JSON.parse(this.users)['phone'];
    this.userCredentials.firstname = JSON.parse(this.users)['firstname'];
    this.userCredentials.lastname = JSON.parse(this.users)['lastname'];
  }
}
