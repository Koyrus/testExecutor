import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from "@angular/forms";
import {empty} from "rxjs/Observer";
import {Http, RequestOptions , Headers} from "@angular/http";
import {GlobalVariable} from "../global";

@Component({
  selector: 'app-newuser',
  templateUrl: './newuser.component.html',
  styleUrls: ['./newuser.component.css']
})
export class NewuserComponent implements OnInit {
  public baseApiUrl = GlobalVariable.globalApiUrl;

  usernameUser : any;
  passwordUser : any;
  userEmail : any;
  userIdnp : any;
  userPhone : any;
  userName : any;
  userNameLast : any;
  roleUser : number = null;
  organizationUser : any;
  userFunctions : any;
  checkedRole : any;
  employeeSelected: boolean =false;

  userresults : any;
  userRoles : any;
  userWorkFunctions : any;
  userOrganizations : any;

  constructor(private formBuilder: FormBuilder , private http : Http) { }

  isEmployee(roleUsr){
    if(roleUsr != null){
      return roleUsr.is_employe;
    }
    // if(!roleUsr)
    //   return false;
    // for(let i = 0; i < this.userRoles.length; i++){
    //   if(this.userRoles[i].id === roleUsr){
    //     return this.userRoles[i].is_employe;
    //   }
    // }
  }
  // form: FormGroup;
  ngOnInit() {
    // this.form = this.formBuilder.group({
    //   email: [null, [Validators.required, Validators.email]],
    //   password: [null, Validators.required],
    // });
    this.userresults = localStorage.getItem("users");
    console.log(JSON.parse(this.userresults) ,  "roles!!!!");

    this.userRoles = JSON.parse(this.userresults)['roles'];
    console.log(this.userRoles , "roles");

    this.userOrganizations = JSON.parse(this.userresults)['organizations'];
    console.log(this.userOrganizations , "Organizations!!");

    this.userWorkFunctions = JSON.parse(this.userresults)['work_functions'];
    console.log(this.userWorkFunctions);


    // for (let i = 0; i < this.userRoles.length; i++) {
    //   this.checkedRole = JSON.parse(this.userresults)['roles'][i]['is_employe'];
    //   console.log(this.checkedRole , "checked");
    // }

  }

  userCreateSave(){
    let headers = new Headers();
    headers.append("Accept", 'application/json');
    headers.append('Content-Type', 'application/json' );
    let options = new RequestOptions({ headers: headers });

    // let roleParam = {
    //   id : this.roleUser
    // };
    let userOrganizations = {
      id : parseInt(this.organizationUser)
    };
    let userFunctions = {
      id : parseInt(this.userFunctions)
    };

    let postParams = {
      id_login_user: -1,
      role : this.roleUser,
      id_user: null,
      email: this.userEmail,
      firstname: this.userName,
      phone : this.userPhone,
      idnp: this.userIdnp,
      is_active: true,
      lastname: this.userNameLast,
      // roles : [roleParam],
      organizations : [userOrganizations.id != null ?  userOrganizations:null],
      work_functions : [userFunctions.id != null ? userFunctions:null],
      password : this.passwordUser,
      username : this.usernameUser
    };
    this.http.post(this.baseApiUrl+"/executor/admin/users/save" , postParams , options)
      .subscribe(res =>{
      console.log(res);
      console.log(postParams , "12312312");
    })
  }
}
