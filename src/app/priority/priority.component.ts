import { Component, OnInit } from '@angular/core';
import {Http, RequestOptions , Headers} from "@angular/http";
import {Router} from "@angular/router";
import {ConfirmationService, Message} from "primeng/primeng";
import {GlobalVariable} from "../global";

@Component({
  selector: 'app-priority',
  templateUrl: './priority.component.html',
  styleUrls: ['./priority.component.css']
})
export class PriorityComponent implements OnInit {
  public baseApiUrl = GlobalVariable.globalApiUrl;

  public priority_result : any[]=[];
  display: boolean = false;
  msgs: Message[] = [];
  displayDialog: boolean;
  public priority_edit : any[]=[];
  public new_priority : boolean = false;
  public priority_nom : any[]=[];

  constructor(private http : Http ,private router : Router , private confirmationService: ConfirmationService) {

  }

  ngOnInit() {
    var thisref = this;
    this.http.get(this.baseApiUrl+"/executor/admin/nomenclator/priority" , {
    }).subscribe(function (res) {
      console.log(res);
      thisref.priority_result = res.json();
      console.log(thisref.priority_result , "priority");

    })
  }
  showDialog(priority) {
    if(priority != null){
      this.priority_edit = priority;
      this.new_priority = false;
    }else {
      this.priority_edit = [];
      this.new_priority = true;
    }
    this.display = true;
  }

  saveProirity(){
    var thisref = this;
    var headers = new Headers();
    headers.append("Accept", 'application/json');
    headers.append('Content-Type', 'application/json' );
    let options = new RequestOptions({ headers: headers });
    let postParams = {
      id_login_user: -1,
      id_nomenclator: this.priority_edit['id'],
      name : this.priority_edit['name'],
      type_nomenclator: "PRIORITY",

    };

    console.log(postParams , "postParams");


    this.http.post( this.baseApiUrl+"/executor/admin/nomenclator/save", postParams , options).subscribe(
      response => {
        this.priority_nom = response.json();
        if(this.new_priority){
          this.priority_result.push(this.priority_nom);
          this.msgs = [];
          this.msgs.push({severity:'success', summary:'Info Message', detail:'Successfuly'});
        }

      }, error => {
        console.log("err");
        this.msgs = [];
        this.msgs.push({severity:'error', summary:'Info Message', detail:'Error'});
      }
    );
    this.display = false;
    console.log(this.priority_result);
  }



  public activatedNomPayment(priority){
    console.log(priority, "user");
    var headers = new Headers();
    headers.append("Accept", 'application/json');
    headers.append('Content-Type', 'application/json' );
    let options = new RequestOptions({ headers: headers });
    let postParams = {
      id_login_user: -1,
      id_nomenclator: priority.id,
      is_active: priority.is_active,
      type_nomenclator: "PAYMENT_MODE",
    };
    console.log(!priority.is_active, "!organization.is_active");
    this.http.post(this.baseApiUrl+"/executor/admin/nomenclator/active", postParams , options).subscribe(function (res) {
      console.log(res);
    });
  }


  deletePriority(priority) {
    this.confirmationService.confirm({
      message: 'Do you want to delete this record?',
      header: 'Delete Confirmation?',
      icon: 'fa fa-trash',
      accept: () => {
        let postParams = {
          id_login_user: -1,
          id_nomenclator: priority.id,
          is_delete: true
        };
        var headers = new Headers();
        headers.append("Accept", 'application/json');
        headers.append('Content-Type', 'application/json' );
        let options = new RequestOptions({ headers: headers });
        console.log(postParams , "postParams");
        this.http.post(this.baseApiUrl+"/executor/admin/nomenclator/delete", postParams , options).subscribe(function (res) {
          console.log(res);
        });
        let index = this.priority_result.indexOf(priority);
        console.log(index, "index");
        this.priority_result = this.priority_result.filter((val, i) => i != index);
        this.displayDialog = false;
        this.msgs = [{severity: 'info', summary: 'Confirmed', detail: 'Record deleted'}];
      },
      reject: () => {
        this.msgs = [{severity: 'info', summary: 'Rejected', detail: 'You have rejected'}];
      }
    });
  }


}
