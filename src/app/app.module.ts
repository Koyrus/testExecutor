import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {AppComponent} from './app.component';
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {DashboardComponent} from './dashboard/dashboard.component';
import {RouterModule} from "@angular/router";
import {LoginComponent} from './login/login.component';
import {DosareStatComponent} from './dosare-stat/dosare-stat.component';
import {Ng2TableModule} from 'ng2-table/ng2-table';
import {PaginationModule} from 'ng2-bootstrap/ng2-bootstrap';
import {HttpModule} from "@angular/http";
import {DataTablesModule} from "angular-datatables";
import {
  CalendarModule, DataTableModule, DropdownModule, Growl, GrowlModule, LazyLoadEvent, Message,
  MultiSelectModule
} from "primeng/primeng";
import {DosarePensiiAlimentareComponent} from './dosare-pensii-alimentare/dosare-pensii-alimentare.component';
import {DashcomponentComponent} from './dashcomponent/dashcomponent.component';
import {EventdataComponent} from './eventdata/eventdata.component';
import {AccountsettingsComponent} from './accountsettings/accountsettings.component';
import {AdminComponent} from './admin/admin.component';
import {AdminLoginComponent} from './adminDashboard/admin-login.component';
import {RolesComponent} from './roles/roles.component';
import {EditdosarComponent} from './editdosar/editdosar.component';
import {FormWizardModule} from "angular2-wizard/dist";
import {DatepickerModule} from "ng2-bootstrap";
import {ConfirmDialogModule, ConfirmationService} from "primeng/primeng";
import {ScheduleModule} from 'primeng/primeng';
import {FullCalendar} from "fullcalendar/dist/fullcalendar";
import {DialogModule} from 'primeng/primeng';
import {CheckboxModule} from 'primeng/primeng';
import {MessagesModule} from 'primeng/primeng';
import {FileUploadModule} from 'primeng/primeng';
import {PanelModule} from 'primeng/primeng';
import {AutoCompleteModule} from 'primeng/primeng';
import {TabViewModule} from 'primeng/primeng';





import {
  MatAutocompleteModule,
  MatButtonModule,
  MatButtonToggleModule,
  MatCardModule,
  MatDatepickerModule,
  MatNativeDateModule,

} from '@angular/material';
import {CdkTableModule} from "@angular/cdk/table";
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import {Eventservice} from "./eventdata/eventservice";
import { OrganizationsComponent } from './organizations/organizations.component';
import { CreateOrganizationComponent } from './create-organization/create-organization.component';
import { UsersComponent } from './users/users.component';
import {NewuserComponent} from "./newuser/newuser.component";
import { NewroleComponent } from './newrole/newrole.component';
import { EditOrganizationComponent } from './edit-organization/edit-organization.component';
import { EditRolesComponent } from './edit-roles/edit-roles.component';
import { ProfilesComponent } from './profiles/profiles.component';
import { EditUserComponent } from './edit-user/edit-user.component';
import { NomenclatoareComponent } from './nomenclatoare/nomenclatoare.component';
import { WorkFunctionsComponent } from './work-functions/work-functions.component';
import { PaymentComponent } from './payment/payment.component';
import { NomcityComponent } from './nomcity/nomcity.component';
import { InstanceEmitentComponent } from './instance-emitent/instance-emitent.component';
import { PriorityComponent } from './priority/priority.component';
import { EditDosarePensiiAlimentareComponent } from './edit-dosare-pensii-alimentare/edit-dosare-pensii-alimentare.component';
import { TipDocumentComponent } from './tip-document/tip-document.component';
import {MyDateAdapter} from "./edit-dosare-pensii-alimentare/date-format";
import {HashLocationStrategy, LocationStrategy} from "@angular/common";
import { DosarePensiiEditStepsComponent } from './dosare-pensii-edit-steps/dosare-pensii-edit-steps.component';
import {DetaliiDosar} from './detalii-dosar/detalii-dosar';
import {FieldsetModule} from 'primeng/primeng';
import { ObjectExecutionComponent } from './object-execution/object-execution.component';
import { DosareCivileComponent } from './dosare-civile/dosare-civile.component';
import {EditCaseCivilComponent} from './dosar-civil-create/edit-case-civil.component';
import { CreateDosareStatComponent } from './create-dosare-stat/create-dosare-stat.component';
import { DosareMeleComponent } from './dosare-mele/dosare-mele.component';


const routes =
  [
    {path: "admin", component: AdminComponent},
    {
      path: 'adminDashboard', component: AdminLoginComponent, children:
      [
        {path: 'roles', component: RolesComponent},
        {path : 'organization' , component : OrganizationsComponent},
        {path : 'createOrganization' , component : CreateOrganizationComponent},
        {path : 'users'  , component : UsersComponent},
        {path : 'newuser' , component : NewuserComponent},
        {path : 'roles/:newrole' , component : NewroleComponent},
        {path : 'editOrganization/:id' , component : EditOrganizationComponent , pathMatch:'full'},
        {path : 'editRole/:id' , component : EditRolesComponent , pathMatch : 'full'},
        {path : 'profiles' , component : ProfilesComponent},
        {path : 'editUser/:id' , component : EditUserComponent},
        {path : 'nomenclatorBanks' , component : NomenclatoareComponent},
        {path : 'workFunctions' , component : WorkFunctionsComponent},
        {path : 'nomenclatorCities' , component : NomcityComponent},
        {path : 'priority' , component : PriorityComponent},
        {path : 'payment' , component : PaymentComponent},
        {path : 'instanseEmitent' , component : InstanceEmitentComponent},
        {path : 'tip-document' , component : TipDocumentComponent},
        {path : 'object-execution' , component : ObjectExecutionComponent}

      ]
    },
    {path: '', component: LoginComponent},
    {
      path: 'dash', component: DashboardComponent, children: [
      {path: 'dosareStat', component: DosareStatComponent},
      {path: 'editPensiiAlimentarePA/:id', component: EditDosarePensiiAlimentareComponent, pathMatch:'full'},
      {path: 'dosarePensiiAlimentare', component: DosarePensiiAlimentareComponent},
      {path: 'calendar', component: EventdataComponent},
      {path: 'accountSettings', component: AccountsettingsComponent},
      {path: 'editPensiiAlimentare/:name', component: EditDosarePensiiAlimentareComponent, pathMatch:'full'},
      {path : 'detalii-dosar/:name' , component : DetaliiDosar , pathMatch:'full'},
      //////////////////////Dosare Civile////////////////
      {path : 'dosare-civile' , component : DosareCivileComponent},
      {path : 'dosare-civileCreate' , component : EditCaseCivilComponent},
      {path : 'dosare-civileCreate/:name' , component : EditCaseCivilComponent},
      //////////////////////Dosare Stat///////////////////
      {path : 'DosareStatCreate' , component : CreateDosareStatComponent},
      {path : 'DosareStatCreate/:name' , component : CreateDosareStatComponent},
      {path : 'DosareMele' , component : DosareMeleComponent}

    ]
    },
  ];

@NgModule({
  declarations: [
    AppComponent,
    DashboardComponent,
    LoginComponent,
    DosareStatComponent,
    DosarePensiiAlimentareComponent,
    DashcomponentComponent,
    EventdataComponent,
    AccountsettingsComponent,
    AdminComponent,
    AdminLoginComponent,
    RolesComponent,
    EditdosarComponent,
    OrganizationsComponent,
    CreateOrganizationComponent,
    UsersComponent,
    NewuserComponent,
    NewroleComponent,
    EditOrganizationComponent,
    EditRolesComponent,
    ProfilesComponent,
    EditUserComponent,
    NomenclatoareComponent,
    WorkFunctionsComponent,
    PaymentComponent,
    NomcityComponent,
    InstanceEmitentComponent,
    PriorityComponent,
    EditDosarePensiiAlimentareComponent,
    TipDocumentComponent,
    DosarePensiiEditStepsComponent,
    DetaliiDosar,
    ObjectExecutionComponent,
    DosareCivileComponent,
    EditCaseCivilComponent,
    CreateDosareStatComponent,
    DosareMeleComponent,


  ],
  imports: [
    BrowserModule,
    FormsModule,
    BrowserAnimationsModule,
    RouterModule.forRoot(routes),
    ReactiveFormsModule,
    Ng2TableModule,
    PaginationModule,
    HttpModule,
    DataTablesModule,
    DataTableModule,
    FormWizardModule,
    DatepickerModule.forRoot(),
    MatNativeDateModule,
    ReactiveFormsModule,
    CdkTableModule,
    MatAutocompleteModule,
    MatButtonModule,
    MatButtonToggleModule,
    MatCardModule,
    MatDatepickerModule,
    ConfirmDialogModule,
    ScheduleModule,
    CalendarModule,
    DialogModule,
    CheckboxModule,
    MessagesModule,
    FileUploadModule,
    GrowlModule,
    PanelModule,
    AutoCompleteModule,
    TabViewModule,
    FieldsetModule,
    MultiSelectModule,
    DropdownModule

  ],
  providers: [ConfirmationService , {provide: LocationStrategy, useClass: HashLocationStrategy}],
  bootstrap: [AppComponent]
})
export class AppModule {
}
