import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DosareMeleComponent } from './dosare-mele.component';

describe('DosareMeleComponent', () => {
  let component: DosareMeleComponent;
  let fixture: ComponentFixture<DosareMeleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DosareMeleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DosareMeleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
