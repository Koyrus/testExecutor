import {AfterViewInit, Component, OnInit, Pipe} from '@angular/core';
import {Http, RequestOptions , Headers} from "@angular/http";
import {Router} from "@angular/router";
import {GlobalVariable} from "../global"

@Component({
  selector: 'app-create-organization',
  templateUrl: './create-organization.component.html',
  styleUrls: ['./create-organization.component.css']
})

export class CreateOrganizationComponent implements OnInit,AfterViewInit {
  public baseApiUrl = GlobalVariable.globalApiUrl;

  ngAfterViewInit(): void {
    jQuery('.ui-panel-titlebar').css('background-color','#4a89dc');
    jQuery('.ui-panel-titlebar').css('color','white');

  }

  public myText: string = '';
  public test : any;
  public banksOrganizations : any[]=[];
  public cityOrganizations : any[] =[];
  bankOrganization : number = null;
  cityOrganization : number = null;
  street : any = null;
  block : any = null;
  bank_accoutId : any = null;
  phone : any = null;
  fax : any = null;
  ngOnInit():void{
    var thisref = this;
    this.test = localStorage.getItem('banks_For_CreateOrganization');
    thisref.banksOrganizations = JSON.parse(this.test)['nomenclator']['banks_nom'];
    thisref.cityOrganizations = JSON.parse(this.test)['nomenclator']['cities_nom'];
    console.log(thisref.banksOrganizations , "b!!!!!!!!!!!!!!");
    console.log(thisref.cityOrganizations , "ccccc!!!!!!!!!!!!!!");
  }

  dateCredentials = {
    code: null,
    idno: null,
    email: null,
    city: null,
    addressa: null ,
    post_code: null ,
    hoursw: null ,
    bank: null ,
    id : null,
    login_user_id : null,
    name : null,
    phone : null
  };
  constructor(private http : Http , private router : Router) { }



  public sendDate(){
    var headers = new Headers();
    headers.append("Accept", 'application/json');
    headers.append('Content-Type', 'application/json' );
    let options = new RequestOptions({ headers: headers });



    let postParams = {
      "id": null,
      "id_login_user": -1,
      "licence_code": this.dateCredentials.code,
      "name": this.dateCredentials.name,
      "idno": this.dateCredentials.idno,
      "city": this.cityOrganization,
      "street": this.street,
      "block":this.block,
      // "fiscal_code": 432423423432,
      "bank_account": this.bank_accoutId,
      "bank_name_id": this.bankOrganization,
      // "other_bank_name": "my bank",
      "email": this.dateCredentials.email,
      "post_code": this.dateCredentials.post_code,
      // "working_hours": "9:30-9:35",
       "phone": this.phone,
      "fax" : this.fax
    };
    console.log(postParams);
    this.http.post(this.baseApiUrl+"executor/admin/organization/save" , postParams , options ).subscribe(res =>{
      console.log(res);
    });

    this.router.navigateByUrl("adminDashboard/organization");
  }

}
