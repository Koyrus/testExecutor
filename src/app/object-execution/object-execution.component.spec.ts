import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ObjectExecutionComponent } from './object-execution.component';

describe('ObjectExecutionComponent', () => {
  let component: ObjectExecutionComponent;
  let fixture: ComponentFixture<ObjectExecutionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ObjectExecutionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ObjectExecutionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
