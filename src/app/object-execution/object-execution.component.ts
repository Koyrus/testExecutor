
import { Component, OnInit } from '@angular/core';
import {Http, RequestOptions , Headers} from "@angular/http";
import {Router} from "@angular/router";
import {ConfirmationService, Message} from "primeng/primeng";
import {GlobalVariable} from "../global";

@Component({
  selector: 'app-object-execution',
  templateUrl: './object-execution.component.html',
  styleUrls: ['./object-execution.component.css']
})
export class ObjectExecutionComponent implements OnInit {
  public baseApiUrl = GlobalVariable.globalApiUrl;

  public object_result : any[]=[];
  display: boolean = false;
  msgs: Message[] = [];
  displayDialog: boolean;
  public object_edit : any[]=[];
  public new_object : boolean = false;
  public object_nom : any[]=[];
  public testForFor : any[]=[];
  public objectExecutionFor : any[]=[];
  public object_execution_list : any[]=[];

  constructor(private http : Http ,private router : Router , private confirmationService: ConfirmationService) {

  }

  ngOnInit() {
    var thisref = this;
    this.http.get(this.baseApiUrl + '/executor/admin/nomenclator/object/execution' , {
    }).subscribe(function (res) {
      console.log(res.json());
      thisref.object_execution_list = res.json()['object_execution_list'];
      thisref.object_result['obj_execute_type'] = res.json();
      thisref.testForFor.push(thisref.object_result);
      thisref.objectExecutionFor = thisref.testForFor[0]['obj_execute_type']['obj_execute_type'];
      console.log(thisref.objectExecutionFor , '******************');

    })
  }
  showDialog(execution) {
    if(execution != null){
      this.object_edit = execution;
      this.new_object = false;
    }else {
      this.object_edit = [];
      this.new_object = true;
    }
    this.display = true;
  }

  saveObject(){
    var thisref = this;
    var headers = new Headers();
    headers.append("Accept", 'application/json');
    headers.append('Content-Type', 'application/json' );
    let options = new RequestOptions({ headers: headers });
    let postParams = {
      id_login_user: -1,
      name: thisref.object_edit['name'],
      obj_execution_type: thisref.object_edit['type'],
      is_amount:true,
      description : thisref.object_edit['description'],
      id_obj_execution : null,

    };

    console.log(postParams , "postParams");


    this.http.put( this.baseApiUrl + '/executor/admin/nomenclator/object/execution/save', postParams , options).subscribe(
      response => {
        this.object_nom = response.json();
        if(this.new_object){
          this.object_result.push(this.object_nom);
          this.msgs = [];
          this.msgs.push({severity:'success', summary:'Info Message', detail:'Successfuly'});
        }

      }, error => {
        console.log("err");
        this.msgs = [];
        this.msgs.push({severity:'error', summary:'Info Message', detail:'Error'});
      }
    );
    this.display = false;
    console.log(this.object_result);
  }



  public activatedNomObject(priority){
    console.log(priority, "user");
    var headers = new Headers();
    headers.append("Accept", 'application/json');
    headers.append('Content-Type', 'application/json' );
    let options = new RequestOptions({ headers: headers });
    let postParams = {
      // id_login_user: -1,
      // id_nomenclator: priority.id,
      // is_active: priority.is_active,
      // type_nomenclator: "PAYMENT_MODE",
    };
    console.log(!priority.is_active, "!organization.is_active");
    this.http.post(this.baseApiUrl + '/executor/admin/nomenclator/object/execution', postParams , options).subscribe(function (res) {
      console.log(res);
    });
  }


  deleteObject(priority) {
    this.confirmationService.confirm({
      message: 'Do you want to delete this record?',
      header: 'Delete Confirmation?',
      icon: 'fa fa-trash',
      accept: () => {
        let postParams = {
          id_login_user: -1,
          id_nomenclator: priority.id,
          is_delete: true
        };
        var headers = new Headers();
        headers.append("Accept", 'application/json');
        headers.append('Content-Type', 'application/json' );
        let options = new RequestOptions({ headers: headers });
        console.log(postParams , "postParams");
        this.http.post(this.baseApiUrl+"/executor/admin/nomenclator/delete", postParams , options).subscribe(function (res) {
          console.log(res);
        });
        let index = this.object_result.indexOf(priority);
        console.log(index, "index");
        this.object_result = this.object_result.filter((val, i) => i != index);
        this.displayDialog = false;
        this.msgs = [{severity: 'info', summary: 'Confirmed', detail: 'Record deleted'}];
      },
      reject: () => {
        this.msgs = [{severity: 'info', summary: 'Rejected', detail: 'You have rejected'}];
      }
    });
  }


}
