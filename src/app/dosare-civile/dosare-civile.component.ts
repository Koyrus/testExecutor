import {AfterViewInit, Component, OnInit} from '@angular/core';
import {Http, RequestOptions , Headers} from "@angular/http";
import {Router} from "@angular/router";
import {GlobalVariable} from "../global";
import {ConfirmationService, Message} from "primeng/primeng";
import {toInt} from "ngx-bootstrap/bs-moment/utils/type-checks";

@Component({
  selector: 'app-dosare-civile',
  templateUrl: './dosare-civile.component.html',
  styleUrls: ['./dosare-civile.component.css']
})
export class DosareCivileComponent implements OnInit,AfterViewInit {
  ngAfterViewInit(): void {
    jQuery('.ui-inputtext').css
    ({
      'height': '2rem',
      'border-top-color': 'transparent',
      'border-left-color': 'transparent',
      'border-right-color': 'transparent',
      'transition': 'unset',
      '-webkit-transition': 'unset',
      'box-sizing': 'unser',
      '-webkit-box-sizing':'unser'
    })
  }
  public baseApiUrl = GlobalVariable.globalApiUrl;
  public usersPa : any[]=[];
  public userrs : any[]=[];
  public id_dosar : string = 'new';
  public pensiiAlimentare : any[];
  msgs: Message[] = [];
  displayDialog: boolean;
  constructor(private http : Http , private router : Router,private confirmationService: ConfirmationService ) {
    var thisref = this;
    this.usersPa=[];
    var headers = new Headers();
    headers.append("Accept", 'application/json');
    headers.append('Content-Type', 'application/json' );
    headers.append('Cache-Control', 'no-cache');

    let options = new RequestOptions({ headers: headers });
    let postParams = {
      id_login_user: toInt(GlobalVariable.login_id),
    };

    this.http.post(this.baseApiUrl+"/executor/civil/case/all" , postParams , options).subscribe(function (res) {
      thisref.usersPa=res.json();
    });
  }

  detaliiDosar(id : number){
    this.router.navigateByUrl('/dash/detalii-dosar/'+id)
  }
  deleteDosar(dosar){
    this.confirmationService.confirm({
      message: 'Sigur doriți să ștergeți ?',
      header: 'Confirmare',
      icon: 'fa fa-trash',
      accept: () => {
        let postParams = {
          id_login_user: toInt(GlobalVariable.login_id),
          id_case: dosar.id_case
        };
        var headers = new Headers();
        headers.append("Accept", 'application/json');
        headers.append('Content-Type', 'application/json' );
        let options = new RequestOptions({ headers: headers });

        this.http.post(this.baseApiUrl+"/executor/case/delete", postParams , options).subscribe(function (res) {
          console.log(res);
          console.log(postParams)

        });
        let index = this.usersPa.indexOf(dosar);
        console.log(index, "index");
        this.usersPa = this.usersPa.filter((val, i) => i != index);
        this.displayDialog = false;
        this.msgs = [{severity: 'success', summary: 'Confirmarea', detail: 'Succes'}];
      },
      reject: () => {
        // this.msgs = [{severity: 'info', summary: 'Rejected', detail: 'You have rejected'}];
      }
    });
  }

  createDosarCivil(){
    this.router.navigateByUrl('/dash/dosare-civileCreate');
  }
  editDosarCivil(id : number){
    this.router.navigateByUrl('/dash/dosare-civileCreate/'+id);
    console.log(id);
  }
  ngOnInit() {

  }

}
