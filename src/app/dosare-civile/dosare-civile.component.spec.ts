import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DosareCivileComponent } from './dosare-civile.component';

describe('DosareCivileComponent', () => {
  let component: DosareCivileComponent;
  let fixture: ComponentFixture<DosareCivileComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DosareCivileComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DosareCivileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
