import { Component, OnInit } from '@angular/core';
import {ConfirmationService, Message} from "primeng/primeng";
import {Router} from "@angular/router";
import {Http, RequestOptions  ,Headers} from "@angular/http";
import {GlobalVariable} from "../global";

@Component({
  selector: 'app-tip-document',
  templateUrl: './tip-document.component.html',
  styleUrls: ['./tip-document.component.css']
})
export class TipDocumentComponent implements OnInit {
  public baseApiUrl = GlobalVariable.globalApiUrl;

  public document_result : any[] = [];
  msgs: Message[] = [];
  display: boolean = false;
  public doc_type_edit : any[]=[];
  public new_doc_type_priority : boolean = false;
  public doc_type_nom : any[]=[];
  displayDialog: boolean;

  constructor(private http : Http ,private router : Router , private confirmationService: ConfirmationService) {

  }

  ngOnInit() {
    var thisref = this;
    this.http.get(this.baseApiUrl+'/executor/admin/nomenclator/doctype').subscribe(function (res) {
      console.log(res , "type");
      thisref.document_result = res.json();

    })
  }
  showDialog(type) {
    if(type != null){
      this.doc_type_edit = type;
      this.new_doc_type_priority = false;
    }else {
      this.doc_type_edit = [];
      this.new_doc_type_priority = true;
    }
    this.display = true;
  }


  saveType(){
    var thisref = this;
    var headers = new Headers();
    headers.append("Accept", 'application/json');
    headers.append('Content-Type', 'application/json' );
    let options = new RequestOptions({ headers: headers });
    let postParams = {
      id_login_user: -1,
      id_nomenclator: this.doc_type_edit['id'],
      name : this.doc_type_edit['name'],
      type_nomenclator: "DOC_TYPE",

    };

    console.log(postParams , "postParams");


    this.http.post( this.baseApiUrl+"/executor/admin/nomenclator/save", postParams , options).subscribe(
      response => {
        this.doc_type_nom = response.json();
        if(this.new_doc_type_priority){
          this.document_result.push(this.doc_type_nom);
          this.msgs = [];
          this.msgs.push({severity:'success', summary:'Info Message', detail:'Successfuly'});
        }

      }, error => {
        console.log("err");
        this.msgs = [];
        this.msgs.push({severity:'error', summary:'Info Message', detail:'Error'});
      }
    );
    this.display = false;
    console.log(this.document_result);
  }


  public activatedNomType(document){
    console.log(document, "user");
    var headers = new Headers();
    headers.append("Accept", 'application/json');
    headers.append('Content-Type', 'application/json' );
    let options = new RequestOptions({ headers: headers });
    let postParams = {
      id_login_user: -1,
      id_nomenclator: document.id,
      is_active: document.is_active,
      type_nomenclator: "PAYMENT_MODE",
    };
    console.log(!document.is_active, "!organization.is_active");
    this.http.post(this.baseApiUrl+"/executor/admin/nomenclator/active", postParams , options).subscribe(function (res) {
      console.log(res);
    });


}

  deleteType(document) {
    this.confirmationService.confirm({
      message: 'Do you want to delete this record?',
      header: 'Delete Confirmation?',
      icon: 'fa fa-trash',
      accept: () => {
        let postParams = {
          id_login_user: -1,
          id_nomenclator: document.id,
          is_delete: true
        };
        var headers = new Headers();
        headers.append("Accept", 'application/json');
        headers.append('Content-Type', 'application/json' );
        let options = new RequestOptions({ headers: headers });
        console.log(postParams , "postParams");
        this.http.post(this.baseApiUrl+"/executor/admin/nomenclator/delete", postParams , options).subscribe(function (res) {
          console.log(res);
        });
        let index = this.document_result.indexOf(document);
        console.log(index, "index");
        this.document_result = this.document_result.filter((val, i) => i != index);
        this.displayDialog = false;
        this.msgs = [{severity: 'info', summary: 'Confirmed', detail: 'Record deleted'}];
      },
      reject: () => {
        this.msgs = [{severity: 'info', summary: 'Rejected', detail: 'You have rejected'}];
      }
    });
  }

}
