import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WorkFunctionsComponent } from './work-functions.component';

describe('WorkFunctionsComponent', () => {
  let component: WorkFunctionsComponent;
  let fixture: ComponentFixture<WorkFunctionsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WorkFunctionsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WorkFunctionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
