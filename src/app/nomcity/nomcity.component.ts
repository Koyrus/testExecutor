import { Component, OnInit } from '@angular/core';
import {ConfirmationService} from "primeng/primeng";
import {Router} from "@angular/router";
import {Http, RequestOptions , Headers} from "@angular/http";
import {Message} from "primeng/components/common/message";
import {GlobalVariable} from "../global";

@Component({
  selector: 'app-nomcity',
  templateUrl: './nomcity.component.html',
  styleUrls: ['./nomcity.component.css']
})
export class NomcityComponent implements OnInit {
  public baseApiUrl = GlobalVariable.globalApiUrl;

  cities_result :any[]=[];
  public city_edit : any[]=[];
  public new_city : boolean = false;
  msgs: Message[] = [];
  displayDialog: boolean;
  display: boolean = false;
  public cities_nom : any[]=[];

  constructor(private http : Http ,private router : Router , private confirmationService: ConfirmationService) { }

  ngOnInit() {
    var thisref = this;
    this.http.get(thisref.baseApiUrl +"/executor/admin/nomenclator/cities" , {
    }).subscribe(function (res) {
      thisref.cities_result = res.json();

    })
  }

  saveCity(){
    var thisref = this;
    var headers = new Headers();
    headers.append("Accept", 'application/json');
    headers.append('Content-Type', 'application/json' );
    let options = new RequestOptions({ headers: headers });
    let postParams = {
      id_login_user: -1,
      id_nomenclator: this.city_edit['id'],
      name : this.city_edit['name'],
      type_nomenclator: "CITY",

    };



    this.http.post( this.baseApiUrl+"/executor/admin/nomenclator/save", postParams , options).subscribe(
      response => {
        this.cities_nom = response.json();
        if(this.new_city){
          this.cities_result.push(this.cities_nom);
          this.msgs = [];
          this.msgs.push({severity:'success', summary:'Info Message', detail:'Successfuly'});
        }

      }, error => {
        this.msgs = [];
        this.msgs.push({severity:'error', summary:'Info Message', detail:'Error'});
      }
    );
    this.display = false;
    console.log(this.cities_result);
  }

  showDialog(city) {
    if(city != null){
      this.city_edit = city;
      this.new_city = false;
    }else {
      this.city_edit = [];
      this.new_city = true;
    }
    this.display = true;
  }

  public activatedNomCity(city){
    console.log(city, "user");
    var headers = new Headers();
    headers.append("Accept", 'application/json');
    headers.append('Content-Type', 'application/json' );
    let options = new RequestOptions({ headers: headers });
    let postParams = {
      id_login_user: -1,
      id_nomenclator: city.id,
      is_active: city.is_active,
      type_nomenclator: "CITY",
    };
    console.log(!city.is_active, "!organization.is_active");
    this.http.post(this.baseApiUrl+"/executor/admin/nomenclator/active", postParams , options).subscribe(function (res) {
      console.log(res);
    });
  }


  deleteCity(city) {
    this.confirmationService.confirm({
      message: 'Do you want to delete this record?',
      header: 'Delete Confirmation?',
      icon: 'fa fa-trash',
      accept: () => {
        let postParams = {
          id_login_user: -1,
          id_nomenclator: city.id,
          is_delete: true
        };
        var headers = new Headers();
        headers.append("Accept", 'application/json');
        headers.append('Content-Type', 'application/json' );
        let options = new RequestOptions({ headers: headers });
        console.log(postParams , "postParams");
        this.http.post(this.baseApiUrl+"/executor/admin/nomenclator/delete", postParams , options).subscribe(function (res) {
          console.log(res);
        });
        let index = this.cities_result.indexOf(city);
        console.log(index, "index");
        this.cities_result = this.cities_result.filter((val, i) => i != index);
        this.displayDialog = false;
        this.msgs = [{severity: 'info', summary: 'Confirmed', detail: 'Record deleted'}];
      },
      reject: () => {
        this.msgs = [{severity: 'info', summary: 'Rejected', detail: 'You have rejected'}];
      }
    });
  }

}
