import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NomcityComponent } from './nomcity.component';

describe('NomcityComponent', () => {
  let component: NomcityComponent;
  let fixture: ComponentFixture<NomcityComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NomcityComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NomcityComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
