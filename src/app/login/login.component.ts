import {Component, NgModule, OnInit} from '@angular/core';
import {Router} from "@angular/router";
import {BrowserModule} from "@angular/platform-browser";
import {GlobalVariable} from "../global";
import {Http, RequestOptions , Headers} from "@angular/http";


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
@NgModule({
  imports : [BrowserModule]
})
export class LoginComponent implements OnInit {
  public baseApiUrl = GlobalVariable.globalApiUrl;
  username : any;
  errorMessage : any;
  password : any;
  public login_data : any[]=[];
  constructor(private router: Router , private http:Http){

  }
  ngOnInit() {
    let thisref = this;
    thisref.errorMessage = '';
  }

  Login(){
    let thisref = this;

    if(this.username !='' && this.password != ''){
      var headers = new Headers();
      headers.append("Accept", 'application/json');
      headers.append('Content-Type', 'application/json' );
      let options = new RequestOptions({ headers: headers });
      let postParams ={
        username : this.username,
        password :this.password
      }
      this.http.post(this.baseApiUrl+'/executor/case/login' , postParams , options).subscribe(function (res) {
        console.log(res);
        if(res.status == 204){
          // thisref.errorMessage = res.json()['user_error'];
          thisref.errorMessage = 'Loginul sau parola sunt incorecte';

        } else{

          thisref.router.navigateByUrl('dash/DosareMele');
          GlobalVariable.login_id = res.json()['id_login_user'];
          // GlobalVariable.global_data_dashboard = res.json();
          localStorage.setItem('first' , JSON.stringify(res.json()));
          localStorage.setItem('id' , GlobalVariable.login_id);
        }
      })


    } else {
      thisref.errorMessage = 'Login și parola sunt obligatorie'
    }






  }
}
