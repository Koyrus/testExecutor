import { Component, OnInit } from '@angular/core';
import {Router} from "@angular/router";
import {GlobalVariable} from "../global";
import {Http, RequestOptions ,Headers} from "@angular/http";
import {ConfirmationService, Message} from "primeng/primeng";
import {toInt} from "ngx-bootstrap/bs-moment/utils/type-checks";

@Component({
  selector: 'app-dosare-stat',
  templateUrl: './dosare-stat.component.html',
  styleUrls: ['./dosare-stat.component.css']
})

export class DosareStatComponent implements OnInit{
  public baseApiUrl = GlobalVariable.globalApiUrl;
  public usersPa : any[]=[];
  public userrs : any[]=[];
  public id_dosar : string = 'new';
  public pensiiAlimentare : any[];
  msgs: Message[] = [];
  displayDialog: boolean;
  constructor(private http : Http , private router : Router,private confirmationService: ConfirmationService ) {

  }
  ngOnInit(): void {
    var thisref = this;
    this.usersPa=[];
    var headers = new Headers();
    headers.append("Accept", 'application/json');
    headers.append('Content-Type', 'application/json' );
    headers.append('Cache-Control', 'no-cache');

    let options = new RequestOptions({ headers: headers });
    let postParams = {
      id_login_user: toInt(GlobalVariable.login_id),
    };

    this.http.post(this.baseApiUrl+"/executor/stat/case/all" , postParams , options).subscribe(function (res) {
      thisref.usersPa=res.json();
    });
  }

  deleteDosar(dosar){
    this.confirmationService.confirm({
      message: 'Sigur doriți să ștergeți ?',
      header: 'Confirmare',
      icon: 'fa fa-trash',
      accept: () => {
        let postParams = {
          id_login_user: toInt(GlobalVariable.login_id),
          id_case: dosar.id_case
        };
        var headers = new Headers();
        headers.append("Accept", 'application/json');
        headers.append('Content-Type', 'application/json' );
        let options = new RequestOptions({ headers: headers });

        this.http.post(this.baseApiUrl+"/executor/case/delete", postParams , options).subscribe(function (res) {
          console.log(res);
          console.log(postParams)

        });
        let index = this.usersPa.indexOf(dosar);
        console.log(index, "index");
        this.usersPa = this.usersPa.filter((val, i) => i != index);
        this.displayDialog = false;
        this.msgs = [{severity: 'success', summary: 'Confirmarea', detail: 'Succes'}];
      },
      reject: () => {
        // this.msgs = [{severity: 'info', summary: 'Rejected', detail: 'You have rejected'}];
      }
    });
  }

  detaliiDosar(id : number){
    this.router.navigateByUrl('/dash/detalii-dosar/'+id)
  }
  createDosarStat(){
    this.router.navigateByUrl('/dash/DosareStatCreate')
  }

  editDosarCivil(id : number){
    this.router.navigateByUrl('/dash/DosareStatCreate/'+id);
    console.log(id);
  }
}
