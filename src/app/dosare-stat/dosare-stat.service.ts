import {Injectable, OnInit} from "@angular/core";
import {Http} from "@angular/http";

@Injectable()
export class DosareStatService {

  constructor(private http : Http){}

  getUsers(){
    return this.http.get('https://randomuser.me/api', {
      params: {
        'inc': 'gender,name,picture,location',
        'results': '150',
        'nat': 'gb'
      }
    })
      .map(function (response) {
        return response.json();
      })
      .map(response => response.results).map(users => {
        return users.map(u =>{
          return{
            name : u.name.first + ' ' + u.name.last,
            image : u.picture.large,
            geo : u.location.city + ' ' + u.location.state + ' ' +  u.location.street
          }
        })
      })
  }
}
