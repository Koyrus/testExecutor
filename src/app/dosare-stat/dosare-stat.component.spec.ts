import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DosareStatComponent } from './dosare-stat.component';

describe('DosareStatComponent', () => {
  let component: DosareStatComponent;
  let fixture: ComponentFixture<DosareStatComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DosareStatComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DosareStatComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
