export interface PersonDtoCreditor {
  index : number,
  id : number;
  birth_date: any,
  block: any,
  city: any,
  first_name: any,
  home_address: any,
  id_city: number,
  idnp: number,
  last_name: any,
  name_org: any,
  patronimic: any,
  phone: string,
  post_code: any,
  street: any,
  type_person: string
}
