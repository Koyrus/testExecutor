import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditCaseCivilComponent } from './edit-case-civil.component';

describe('EditComponent', () => {
  let component: EditCaseCivilComponent;
  let fixture: ComponentFixture<EditCaseCivilComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditCaseCivilComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditCaseCivilComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
