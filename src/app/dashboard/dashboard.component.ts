import {Component, OnInit, ViewChild} from '@angular/core';
import {Router} from "@angular/router";
import {Http, RequestOptions,Headers} from "@angular/http";
import {GlobalVariable} from "../global";

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css'],
})
export class DashboardComponent implements OnInit{
  public baseApiUrl = GlobalVariable.globalApiUrl;
  public globalData = localStorage.getItem('global_data_dashboard');
  // public globalDataName = this.globalData['name_executor'];
  public dash_first : any;
  public dash_seconds : any;
  ngOnInit(): void {
    var test = localStorage.getItem('first');
    console.log(JSON.parse(test));
    this.dash_first = JSON.parse(test)['dash_first'];
    this.dash_seconds = JSON.parse(test)['dash_seconds'];

  }



  constructor(private router : Router , private http : Http) { }


}
