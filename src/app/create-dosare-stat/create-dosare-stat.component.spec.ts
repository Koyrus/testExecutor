import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateDosareStatComponent } from './create-dosare-stat.component';

describe('CreateDosareStatComponent', () => {
  let component: CreateDosareStatComponent;
  let fixture: ComponentFixture<CreateDosareStatComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateDosareStatComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateDosareStatComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
