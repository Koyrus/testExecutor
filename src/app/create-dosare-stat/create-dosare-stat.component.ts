import {Component, OnInit, ViewChild} from '@angular/core';
import {Http, RequestOptions ,Headers} from "@angular/http";
import {ActivatedRoute, Router} from "@angular/router";
import {GlobalVariable} from "../global";
import {FileUpload} from "primeng/primeng";
import {PersonDto} from "../dosar-civil-create/PersonDto";
import {toInt} from "ngx-bootstrap/bs-moment/utils/type-checks";

@Component({
  selector: 'app-create-dosare-stat',
  templateUrl: './create-dosare-stat.component.html',
  styleUrls: ['./create-dosare-stat.component.css']
})
export class CreateDosareStatComponent implements OnInit {
  end_date_case: any;
  @ViewChild("cerere_file") cerere_file: FileUpload;
  public type_debitor : any[];
  public personDtoCase : PersonDto[]=[];
  public choose_file_type : any[]=[];
  public person_debitors : any[]=[];
  public document_type : any;
  public object_type_isactiveGet : boolean = false;
  public type_selected : string = 'FIZIC';
  public baseApiUrl = GlobalVariable.globalApiUrl;
  public idn_debitor : any;
  public name_debitor : any;
  public index : number = 1;
  public indexCred : number = 1;
  public childrens : any[]=[];
  public first_name_debitor : any;
  public last_name_debitor : any;
  public patronimic_debitor : any;
  public case_number :any;
  public created_date : any;
  public state_case : any;
  public idnp_debitor_autocomplete : string = '0';
  public autocomplete_array : any[]=[];
  public priority_mode : any[] = [];
  public emitent_instance : string[]=[];
  public filteredBrands: any[];
  public emitent_resultAutoCompleteCivil : any[]=[];
  public doc_executor : any;
  public selectedPriorityID : number;
  public selectedPriorityEntry;
  public object_execution_list : any[]=[];
  public execution_name:string[]=[];
  public text_procedura : any;
  public amount : any;
  public editDosarStatID : number = null;
  public debitorsArray:any[]=[];
  public case_numberCivil : string;
  public stateTypeDebitor:boolean = true;
  public case_team : any[]=[];


  fillData(caseCivilEdit:any[], thisref:any){
    thisref.state_case = caseCivilEdit['state_case'];

    thisref.created_PA_date = caseCivilEdit['created_date']
    if(caseCivilEdit['id']>0) {
      thisref.id = caseCivilEdit['id'];

      ////First page////
      thisref.uploadedContent = caseCivilEdit['files_content'];
      thisref.emitent_instance = caseCivilEdit['nom_emitent_instance'];
      thisref.doc_executor = caseCivilEdit['doc_executor'];
      thisref.amount = caseCivilEdit['amount'];
      thisref.end_date_case = caseCivilEdit['expired_date_case'];
      thisref.text_procedura = caseCivilEdit['text_procedure'];
      thisref.execution_name = caseCivilEdit['nom_object_execution'];
      thisref.selectedPriorityID = caseCivilEdit['nom_priority']['id'];
      thisref.validIDNp=true;
      thisref.selectedPriorityEntry = caseCivilEdit['nom_priority'];


      thisref.object_type_isactiveGet = caseCivilEdit['nom_object_execution']['is_active'];

    }
  }

  constructor(private http : Http , private router : Router , private route : ActivatedRoute) {
    var thisref = this;


    for (let i = 1; i < 10; i++) {
      this.personDtoCase = [];
      this.personDtoCase['index'] = i;
      this.personDtoCase['type_person'] = 'FIZIC';
      this.person_debitors.push(this.personDtoCase);

    }



    var headers = new Headers();
    headers.append("Accept", 'application/json');
    headers.append('Content-Type', 'application/json' );
    let options = new RequestOptions({ headers: headers });
    this.route.params.subscribe(function(params){
      thisref.editDosarStatID = params['name'];
    });
    let postParamsCivil = {
      id_login_user: toInt(GlobalVariable.login_id),
      id_case_st : null
    };

    if(thisref.editDosarStatID > 0){
      postParamsCivil.id_case_st = toInt(thisref.editDosarStatID);
    }else {
      postParamsCivil.id_case_st = null;
    }


    this.http.post(this.baseApiUrl+'/executor/stat/case/create/edit' , postParamsCivil ,options ).subscribe(function (res) {
      thisref.case_numberCivil = res.json()['case_number'];
      thisref.state_case = res.json()['state_case'];
      thisref.created_date = res.json()['created_date'];
      thisref.case_team = res.json()['case_team'];
      thisref.fillEditPerson(res.json()['debitors'] , thisref.person_debitors);
      thisref.fillData(res.json(),thisref);
    });
  }

  public test:any[]=[];

  public validIDNp:boolean=false;

  myFuncDebitorIDNP(){
    var thisref = this;

    let debitorData=thisref.person_debitors[this.index-1];

    let idnp_dat=debitorData.idnp;
    thisref.idnp_debitor_autocomplete = idnp_dat.length;


    if(thisref.person_debitors[thisref.index-1].idnp.length == 13){
      this.validIDNp=true;
      setTimeout(()=>{
        var headers = new Headers();
        headers.append("Accept", 'application/json');
        headers.append('Content-Type', 'application/json' );
        let options = new RequestOptions({ headers: headers });

        let postData = {
          idnp : toInt(idnp_dat),
          id_login_user: toInt(GlobalVariable.login_id),
        };


        this.http.post(this.baseApiUrl+'/executor/case/idnp/search' , postData , options).subscribe(function (res) {
          thisref.autocomplete_array = res.json();
          if(thisref.autocomplete_array['id'] != null && thisref.autocomplete_array['id'] != ''){
            thisref.autocomplete_array = res.json();
            debitorData['last_name']=thisref.autocomplete_array['last_name'];
            debitorData['idnp']=thisref.autocomplete_array['idnp'];
            debitorData['first_name']=thisref.autocomplete_array['first_name'];
            debitorData['birth_date']=thisref.autocomplete_array['birth_date'];
            debitorData['id']=thisref.autocomplete_array['id'];
            debitorData['patronimic']=thisref.autocomplete_array['patronimic'];

            debitorData['post_code'] = thisref.autocomplete_array['post_code'];
            debitorData['street'] = thisref.autocomplete_array['street'];
            debitorData['block'] = thisref.autocomplete_array['block'];
            debitorData['name_org'] = thisref.autocomplete_array['name_org'];
            // debitorData['city'] = thisref.autocomplete_array['city'];
            debitorData['type_person'] = thisref.autocomplete_array['type_person'];
            debitorData['city'] = 'Chișinău';
            debitorData['home_address']=thisref.autocomplete_array['home_address'];
            debitorData['phone']=thisref.autocomplete_array['phone'];

            thisref.type_selected=thisref.autocomplete_array['type_person'];
            thisref.stateTypeDebitor=true;
          } else {
            debitorData['id'] = null;
            thisref.stateTypeDebitor=false;
          }
        })},1500);
      // thisref.idnp_array.push(idnp_dat);
      // console.log(thisref.idnp_array , '111111111111111');
    }else {

      if (this.validIDNp) {
        thisref.stateTypeDebitor = true;
        this.clearAllfields(this.index,this.person_debitors);
        this.validIDNp = false
      }
    }
  }

  // public disable_buttonNext : boolean;
  //
  // checkIfIdnpUnique(){
  //   let thisref = this;
  //   let arr = thisref.idnp_array;
  //   let sorted_arr = arr.sort();
  //   let results = [];
  //   for (let i = 0; i < arr.length - 1; i++) {
  //     if (sorted_arr[i + 1] == sorted_arr[i]) {
  //       results.push(sorted_arr[i]);
  //       console.log('already exist');
  //       this.disable_buttonNext = false;
  //     } else console.log('ok!');
  //     this.disable_buttonNext = true;
  //
  //   }
  // }


  ngAfterViewInit(): void {
    jQuery('.card-footer').css('background-color','#4a89dc');
    jQuery('.card-footer').css('text-align','right');
    jQuery('.btn .btn-secondary .float-right').css('background-color','red');
    var buttonPrev = jQuery('.card-footer button:first').text('Precedent');
    var buttonNext = jQuery('.card-footer button:first').next().text('Următorul');
    var buttonPrev2 = jQuery('.card-footer button:last').text('Salvează');
    jQuery('.ui-inputtext').css({
      'border':'solid 1px',
      'border-color':'#999999',
      'padding-left':'5px',
      'height':'2rem'
    });
  }





  changeTypeDebitor(type_selected , index : number){
    if(type_selected == 'FIZIC'){
      this.person_debitors[index-1].type_person = 'FIZIC';
    }else {
      this.person_debitors[index-1].type_person = 'JURIDIC';
    }
  }


  change_executionType(isAmount:boolean){
    this.object_type_isactiveGet=isAmount;
  }



  ngOnInit() {
    this.http.get(this.baseApiUrl+'/executor/case/nomenclator/doctype').subscribe(function (res) {
      thisref.document_type = res.json();
      thisref.choose_file_type = thisref.document_type;
      console.log(res.json());
    });
    this.http.get(this.baseApiUrl+'/executor/case/nomenclator/priority').subscribe(function (res) {
      thisref.priority_mode = res.json()
    });



    let thisref = this;
    const headers = new Headers();
    headers.append("Accept", 'application/json');
    headers.append('Content-Type', 'application/json' );
    let options = new RequestOptions({ headers: headers });
    let postParams = {
      id_login_user: toInt(GlobalVariable.login_id),
      id_case_st : null,

    };


    this.http.get(this.baseApiUrl+'/executor/case/nomenclator/object/execution/stat').subscribe(function (res) {
      thisref.object_execution_list = res.json();


    })

  }
  choosePersonDebitors(index : number){
    let personChoosed = this.person_debitors[index-1];
    this.type_selected=personChoosed.type_person;
    if(personChoosed.id == null){
      this.stateTypeDebitor=true;
    }

  }




  filterInst(event) {
    this.filteredBrands = null;
    var thisref = this;
    if(this.emitent_instance.length > 1){
      this.http.get(this.baseApiUrl+'/executor/case/instance/search/'+this.emitent_instance)
        .subscribe(function (res) {
          thisref.emitent_resultAutoCompleteCivil = res.json();
        });
    }
  }

  onSelectionChangePriority(priority){
    this.selectedPriorityEntry = priority;
    this.selectedPriorityID = priority.id;

  }
//////////////Больше костылей Богу Костылей!!!////////////////
  selectedTeamEntry;
  selectedTeamID : number = null;
  public array_team_members : any[]=[];
  public unique : any[]=[];
  static onlyUnique(value, index, self) {
    return self.indexOf(value) === index;
  }

  changeTeamMembers(team){
    this.selectedTeamEntry = team;
    this.selectedTeamID = team.id;
    this.selectedTeamEntry['is_selected'] = true;
    this.array_team_members.push(this.selectedTeamEntry);
    for(let i = 0;i <this.array_team_members.length;i++ ){
      var a = this.array_team_members;
      this.unique = a.filter( CreateDosareStatComponent.onlyUnique );
    }
  }
/////////////////Конец Костыля//////////////////////////////

  public uploadedContent : any[]=[];
  public disable_file : boolean = true;
  public uploadCaseFiles : File[]=[];

  onSelectFile(){
    let tempPostTypeDocs = {
      'id_document' : null,
      'file_name' : this.cerere_file.files[0].name,
      'doc_type' : this.choose_file_type['name'],
      'id_doc_type' : this.choose_file_type['id'],
      'is_delete' : false
    };
    this.uploadedContent.push(tempPostTypeDocs);
    this.uploadCaseFiles.push(this.cerere_file.files[0]);
    this.cerere_file.clear();
  }

  deleteCaseFileFromArray(file_select : any){

    if(file_select.id_document!=null && file_select.id_document !=''){
      for(var y=0;y< this.uploadedContent.length;y++){
        let file_elem=this.uploadedContent[y];
        if(file_elem.id_document === file_select.id_document){
          file_elem.is_delete=true;
        }
      }
    }else{
      // this.uploadCaseFiles = this.uploadCaseFiles.filter((val, i) => val.name != file_select.file_name);
      this.uploadedContent = this.uploadedContent.filter((val, i) => val.file_name != file_select.file_name && file_select.id_document == null);
    }

  }

  onChangeFunction(){
    if (this.choose_file_type != null){
      this.disable_file = false;
    }
  }

  finishDosarC(){
    var thisref = this;
    var headers = new Headers();
    headers.append("Accept", 'application/json');
    headers.append('Content-Type', 'application/json' );
    let options = new RequestOptions({ headers: headers });


    this.debitorsArray = this.fillPerson(thisref.person_debitors);
    let idcasecivil = thisref.editDosarStatID  != null ? toInt(thisref.editDosarStatID):null;

    let postParams = {
      //First step/////////////
      id: idcasecivil,
      id_login_user: toInt(GlobalVariable.login_id),
      case_number: this.case_numberCivil,
      state_case: this.state_case,
      doc_executor: this.doc_executor,
      nom_emitent_instance : thisref.emitent_instance,
      files_content : this.uploadedContent,
      doc_type : this.choose_file_type,
      /////////////////////////

      ///Debitor step//////////
      debitors : this.debitorsArray,
      // this.person_jurDeb.concat(this.person_fizDeb),
      /////////////////////////

      // this.person_jurCred.concat(this.person_fizCred),
      //////////////////////

      /////Achitari step//////
      case_team: this.unique,
      text_procedure : this.text_procedura,
      nom_object_execution : this.execution_name,
      amount : parseFloat(this.amount),
      nom_priority : this.selectedPriorityEntry,
      expired_date_case : this.end_date_case
    };
    this.sendDatePAcase(postParams);
    console.log(postParams , '========')
  }


  sendDatePAcase(stepsData : any): void {
    var headers = new Headers();
    let options = new RequestOptions({ headers: headers });
    var form = new FormData();
    for(let i = 0;i < this.uploadCaseFiles.length ; i++){
      form.append("file",  this.uploadCaseFiles[i]);
    }
    form.append('case_st', new Blob([JSON.stringify(stepsData)], {
      type: "application/json"
    }));
    this.http.post(this.baseApiUrl+'/executor/stat/case/save' , form, {
      headers: headers
    }).subscribe(function (res) {

    });
    setTimeout(()=>{  this.router.navigateByUrl("dash/dosareStat");    },3000);


  }



  fillPerson(arrConcat : any[]){
    var thisref = this;

      let array : any[]=[];

      for(let i = 0;i < arrConcat.length;i++){
        if(arrConcat[i]['idnp'] != null ){
          var textTest = {
            id : arrConcat[i]['id'],
            birth_date: arrConcat[i]['birth_date'],
            block: arrConcat[i]['block'] ,
            city: arrConcat[i]['city'],
            first_name: arrConcat[i]['first_name'],
            home_address: arrConcat[i]['home_address'],
            id_city: 1,
            idnp: arrConcat[i]['idnp'],
            last_name: arrConcat[i]['last_name'],
            name_org: arrConcat[i]['name_org'] ,
            patronimic: arrConcat[i]['patronimic'],
            phone: arrConcat[i]['phone'],
            post_code: arrConcat[i]['post_code'],
            street: arrConcat[i]['street'] ,
            type_person: arrConcat[i]['type_person']
          }
          array.push(textTest);
        }
      }
      return array;


  }


  fillEditPerson(arrCaseEdit : any[] , person_types : any[]) {
    for (let i = 0; i < person_types.length; i++) {
      var thisref=this;
      if (thisref.editDosarStatID > 0) {
        if (arrCaseEdit[i] !== undefined) {
          person_types[i]['id'] = arrCaseEdit[i]['id'];
          person_types[i]['birth_date'] = arrCaseEdit[i]['birth_date'];
          person_types[i]['block'] = arrCaseEdit[i]['block'];
          person_types[i]['city'] = arrCaseEdit[i]['city'];
          person_types[i]['first_name'] = arrCaseEdit[i]['first_name'];
          person_types[i]['home_address'] = arrCaseEdit[i]['home_address'];
          person_types[i]['idnp'] = arrCaseEdit[i]['idnp'];
          person_types[i]['last_name'] = arrCaseEdit[i]['last_name'];
          person_types[i]['name_org'] = arrCaseEdit[i]['name_org'];
          person_types[i]['patronimic'] = arrCaseEdit[i]['patronimic'];
          person_types[i]['phone'] = arrCaseEdit[i]['phone'];
          person_types[i]['post_code'] = arrCaseEdit[i]['post_code'];
          person_types[i]['street'] = arrCaseEdit[i]['street'];
          person_types[i]['type_person'] = arrCaseEdit[i]['type_person'];
        }

      }
    }
  }

  clearAllfields(index:number, personsCase:any[]){

    personsCase[this.index-1]['first_name']=null;
    personsCase[this.index-1]['birth_date']=null;
    personsCase[this.index-1]['block']=null;
    personsCase[this.index-1]['city']=null;
    personsCase[this.index-1]['first_name']=null;
    personsCase[this.index-1]['home_address']=null;
    personsCase[this.index-1]['last_name']=null;
    personsCase[this.index-1]['name_org']=null;
    personsCase[this.index-1]['patronimic']=null;
    personsCase[this.index-1]['phone']=null;
    personsCase[this.index-1]['post_code']=null;
    personsCase[this.index-1]['street']=null;


  }

}
